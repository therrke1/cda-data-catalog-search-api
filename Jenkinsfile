pipeline {
    agent any
    tools {
            nodejs 'node-8.2.1'
    }
    triggers {
        pollSCM('H * * * *')
    }
    environment {
        APPLICATION_NAME = 'dcs-api'
        BASE_AMI_ID = 'ami-ee5066f8'
        INSTANCE_TYPE = 't2.micro'
        INSTANCE_IAM_PROFILE = 'Arn=arn:aws:iam::828766338540:instance-profile/api-ec2-iam'
        INSTANCE_AUTO_SCALING_IAM_PROFILE = 'arn:aws:iam::828766338540:instance-profile/api-ec2-iam'
        BRANCH_NAME = "${BRANCH_NAME}"
        SERVICE_NAME = 'api'
        LAUNCH_CONFIGURATION_NAME = "${env.APPLICATION_NAME}-${env.BRANCH_NAME}-${BUILD_NUMBER}"
        AUTO_SCALING_GROUP_NAME_PREFIX = 'dcs-api-asg'
        ASG_MIN_SIZE = 1
        ASG_MAX_SIZE = 4
        ASG_DESIRED_CAPACITY = 2
        KEY_FILE_LOCATION = 's3://nibr-useast1-as-dcs-secret-bucket/keys/dcs-share.pem'
        KEYPAIR_NAME = 'dcs-share'
        CONFIG_FILE_LOCATION = 's3://nibr-useast1-as-dcs-secret-bucket/application/config.json'
        AWS_REGION = 'us-east-1'
        EC2_SUBNET_ID = 'subnet-2b28c271'
        EC2_SECURITY_GROUPS = "sg-4d4c8633#sg-41007c3f#sg-920579ec"
    }
    stages {
        stage('Environment Variables') {
            steps{
               echo "${env.BRANCH_NAME}"
               echo "${BRANCH_NAME} / ${BUILD_NUMBER} / ${BUILD_ID} "
               script {
                GIT_COMMIT = sh (
                    script: 'git rev-parse HEAD',
                    returnStdout: true
                ).trim()

                GIT_URL = sh (
                    script: 'git config --get remote.origin.url',
                    returnStdout: true
                ).trim()
                }
               sh "echo -e '{\"BUILD_TAG\": \"${BUILD_TAG}\", \"BUILD_NUMBER\": \"${BUILD_NUMBER}\", \"GIT_COMMIT\":\"${GIT_COMMIT}\", \"GIT_BRANCH\":\"${BRANCH_NAME}\", \"GIT_URL\":\"${GIT_URL}\"}' > build_info.json"
               sh "cat build_info.json | jq '.'"
            }
        }
        stage('Build') {
            steps{
                parallel (
                    "API Build" : {
                        echo "API Building - Configuration and Install Packages"
                        sh "aws s3 cp ${env.CONFIG_FILE_LOCATION} config.json"
                        sh "cat config.json | jq '.'"
                        sh "npm install"
                    },
                    "UI Build" : {
                        echo "UI Building - Configuration and Install Packages"
                        sh "cd ui && npm install"
                    }
                )
            }
        }
        stage('Testing') {
            steps{
                parallel (
                    "SyntaxCheck" : {
                        echo "Testing Syntax"
                        sh "./node_modules/.bin/eslint -v"
                        sh "npm run lint"
                    },
                    "TestCase" : {
                        echo "Running Test Cases"
                        sh "./node_modules/.bin/nyc --version"
                        sh "npm test"
                    }
                )
            }
        }
        stage('Prompt') {
            steps{
                echo "Prompt to Continue"
                script {
                try {
                    timeout(time: 300, unit: 'SECONDS') {
                    env.RELEASE_SCOPE = input message: 'Continue with Deployment?', ok: 'Release!',
                            parameters: [choice(name: 'RELEASE_SCOPE', choices: 'patch\nminor\nmajor', description: 'What is the release scope?')]
                        }
                    }
                catch(err) {
                    cleanWs()
                    }
                }
            }
        }
        stage('Deploy Development') {
            when {
                expression { return env.RELEASE_SCOPE }
                branch 'feature/*'
            }
            steps{
                sh "aws s3 cp ${env.KEY_FILE_LOCATION} ."
                sh "chmod 400 ${env.KEYPAIR_NAME}.pem"
                sh "devops/jenkins-job.sh ${env.BASE_AMI_ID} ${env.INSTANCE_TYPE} \
                 ${env.INSTANCE_IAM_PROFILE} ${env.INSTANCE_AUTO_SCALING_IAM_PROFILE} \
                 development ${env.SERVICE_NAME} ${env.LAUNCH_CONFIGURATION_NAME} ${env.AUTO_SCALING_GROUP_NAME_PREFIX} \
                 ${env.ASG_MIN_SIZE} ${env.ASG_MAX_SIZE} ${env.ASG_DESIRED_CAPACITY} ${BUILD_NUMBER} \
                 ${JOB_NAME} ${env.AWS_REGION} ${env.EC2_SUBNET_ID} ${env.KEYPAIR_NAME} ${env.APPLICATION_NAME} \
                 ${BRANCH_NAME} ${env.EC2_SECURITY_GROUPS} ${GIT_COMMIT}"
            }
        }
        stage('Deploy Staging') {
            when {
                expression { return env.RELEASE_SCOPE }
                branch 'develop || hotfix/*'
            }
            steps{
                sh "aws s3 cp ${env.KEY_FILE_LOCATION} ."
                sh "chmod 400 ${env.KEYPAIR_NAME}.pem"
                sh "devops/jenkins-job.sh ${env.BASE_AMI_ID} ${env.INSTANCE_TYPE} \
                 ${env.INSTANCE_IAM_PROFILE} ${env.INSTANCE_AUTO_SCALING_IAM_PROFILE} \
                 staging ${env.SERVICE_NAME} ${env.LAUNCH_CONFIGURATION_NAME} ${env.AUTO_SCALING_GROUP_NAME_PREFIX} \
                 ${env.ASG_MIN_SIZE} ${env.ASG_MAX_SIZE} ${env.ASG_DESIRED_CAPACITY} ${BUILD_NUMBER} \
                 ${JOB_NAME} ${env.AWS_REGION} ${env.EC2_SUBNET_ID} ${env.KEYPAIR_NAME} ${env.APPLICATION_NAME} \
                 ${BRANCH_NAME} ${env.EC2_SECURITY_GROUPS} ${GIT_COMMIT}"
            }
        }
        stage('Deploy Production') {
            when {
                expression { return env.RELEASE_SCOPE }
                branch 'master'
            }
            steps{
                sh "aws s3 cp ${env.KEY_FILE_LOCATION} ."
                sh "chmod 400 ${env.KEYPAIR_NAME}.pem"
                sh "devops/jenkins-job.sh ${env.BASE_AMI_ID} ${env.INSTANCE_TYPE} \
                 ${env.INSTANCE_IAM_PROFILE} ${env.INSTANCE_AUTO_SCALING_IAM_PROFILE} \
                 production ${env.SERVICE_NAME} ${env.LAUNCH_CONFIGURATION_NAME} ${env.AUTO_SCALING_GROUP_NAME_PREFIX} \
                 ${env.ASG_MIN_SIZE} ${env.ASG_MAX_SIZE} ${env.ASG_DESIRED_CAPACITY} ${BUILD_NUMBER} \
                 ${JOB_NAME} ${env.AWS_REGION} ${env.EC2_SUBNET_ID} ${env.KEYPAIR_NAME} ${env.APPLICATION_NAME} \
                 ${BRANCH_NAME} ${env.EC2_SECURITY_GROUPS} ${GIT_COMMIT}"
            }
        }
        stage('Cleanup Workspace') {
            when {
                expression { return env.RELEASE_SCOPE }
            }
            steps{
                cleanWs()
            }
        }
    }
}
