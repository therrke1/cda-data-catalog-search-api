### Local Development Checklist
- rename `config.sample.json` to `config.json`
- if `NODE_ENV` is not set in the environment variables - application considers the environment as `local` automatically - so set the `local` db details appropriately in the `config.json` file.

## Minimum Environment Requirements

```
  "engines": {
    "npm": "^5.3.0",
    "node": "^8.2.1"
  }
```

#### AWS CLI Requirement
 
- This API leverages AWS S3 object storage. In order to run the API locally you will need to first set up the AWS Command Line Interface http://docs.aws.amazon.com/cli/latest/userguide/installing.html
- You should have received AWS credentials (key, secrect key), if not please request them from the project's technical lead
- Create a new AWS Config Profile named dcs in your aws.credentials file
- Create and export new environment variable AWS_PROFILE=dcs
 
#### Postgres - if you choose to run an offline copy for development
- https://www.postgresql.org/download/ we are currently version 9.6x

## Build on Branches

| Branch        | Deployable    | Build Link  |
| ------------- |:-------------:| -----:|
| master      | Production | [>-->](https://dcs-jenkins.dev.nibr.novartis.net/job/cda-data-catalog-service-api/job/master/)|
| develop      | Staging      |   [>-->] |
| hotfix | Staging      |    [>-->] |
| feature | Development      |    [>-->] |

