
const express = require('express');
const path = require('path');
const fs = require('fs');
const cookieParser = require('cookie-parser');
const compression = require('compression');
const bodyParser = require('body-parser');
const busboy = require('connect-busboy');
const expressValidator = require('express-validator');
const responseTime = require('response-time');

const logger = require('./middleware/logger');
const cors = require('./middleware/cors');
const auth = require('./middleware/authentication');
const requestLogger = require('./middleware/requestLogger');
const expressValidatorOptions = require('./routes/common/validator').expressValidatorOptions;

const env = process.env.NODE_ENV || 'local';

// Routes Versioning Import
const v1routes = require('./routes/v1/index');
const v2routes = require('./routes/v2/index');

const app = express();
//compress all responses
app.use(compression({
    filter: (req, res) => !(req.query.compress === 'false'),
}));

if (env === 'dev' || env === 'local') {
    app.use(requestLogger);
}

app.use(cookieParser());
app.use(busboy({
    limits: {
        fileSize: 5 * 1024 * 1024 * 1024 * 1024,
    },
}));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(expressValidator(expressValidatorOptions));
app.use(responseTime());

// Swagger
app.use('/docs', express.static(`${__dirname}/docs`));
app.use('/', express.static(`${__dirname}/public`));
app.use(express.static(path.join(__dirname, 'public')));

const SwaggerDiff = require('swagger-diff');
const newSpec = require('./docs/swagger.json');
const oldSpec = require('./docs/previousSwagger.json');

app.get('/swagger/diff', (req, res) => {
    const config = {};
    const prevVersion = oldSpec.info.version;
    SwaggerDiff(oldSpec, newSpec, config).then((diff) => {
        diff.prevVersion = prevVersion;
        res.status(200).json(diff);
    });
});

// Healthcheck
app.get('/health', (req, res) => {
    fs.readFile('build_info.json', 'utf8', (builderr, BuildInfo) => {
        if (builderr) return res.json([]);
        fs.readFile('package.json', 'utf8', (pkgerr, PackageInfo) => {
            if (pkgerr) return res.json([]);
            const ParseBldInfo = JSON.parse(BuildInfo);
            const ParsedPkgInfo = JSON.parse(PackageInfo);

            ParseBldInfo.APP_NAME = ParsedPkgInfo.name;
            ParseBldInfo.VERSION = ParsedPkgInfo.version;
            res.json(ParseBldInfo);
            return null;
        });
        return null;
    });
});


// authenticate and enable cors for all routes
app.all('/*', cors.enable, auth.authenticateUser);

// GraphQL Implementation
const graphqlHTTP = require('express-graphql');
const graphqlRoot = require('./graphql/index');

app.use('/graphql', graphqlHTTP(request => {
    const startTime = Date.now();
    return {
        schema: graphqlRoot.schema,
        graphiql: true,
        extensions({ document, variables, operationName, result }) {
            return { runTime: Date.now() - startTime };
        },
    };
}));

// Routing
const v1 = express.Router();
const v2 = express.Router();

v1.use('/', v1routes);
v2.use('/', v2routes);

app.use('/v1', v1);
app.use('/v2', v2);
app.use('/', v1); // v1 for now - but update it to the latest version as needed

// catch 404 and forward to error handler
app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (process.env.NODE_ENV === 'dev') {
    app.use((err, req, res, next) => {
        res.status(err.status || 500);

        const error = Object.assign({}, err, {
            stack: err.stack ? err.stack.split('\n') : null,
            url: req.url,
            method: req.method,
            user: res.get('userName'),
        });

        logger.error(error);
        res.json(error);
    });
}

// production error handler
// no stacktrace leaked to user
app.use((err, req, res, next) => {
    res.status(err.status || 500);

    logger.error(Object.assign({}, err, {
        stack: err.stack ? err.stack.split('\n') : null,
        url: req.url,
        method: req.method,
        user: res.get('userName'),
    }
  ));

    res.json({
        message: err.message,
        error: {},
    });
});

module.exports = app;
