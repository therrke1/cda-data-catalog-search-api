/* eslint-disable */
const _ = require('lodash');

const objectDiff = typeof exports !== 'undefined' ? exports : {};

/**
 * @param {Object} a
 * @param {Object} b
 * @return {Object}
 */
objectDiff.diff = function diff(a, b) {
  // console.log('a='+a);
  // console.log('b='+b);
    if (a === b) {
        return {
            changed: 'equal',
            value: a,
        };
    }

    const value = {};
    let equal = true;

    for (const key in a) {
        if (key in b) {
            if (a[key] === b[key]) {
                value[key] = {
                    changed: 'equal',
                    value: a[key],
                };
            } else {
                const typeA = typeof a[key];
                const typeB = typeof b[key];
                if (a[key] && b[key] && (typeA === 'object' || typeA === 'function') && (typeB === 'object' || typeB === 'function')) {
                    const valueDiff = diff(a[key], b[key]);
                    if (valueDiff.changed === 'equal') {
                        value[key] = {
                            changed: 'equal',
                            value: a[key],
                        };
                    } else {
                        equal = false;
                        value[key] = valueDiff;
                    }
                } else {
                    equal = false;
                    value[key] = {
                        changed: 'primitive change',
                        removed: a[key],
                        added: b[key],
                    };
                }
            }
        } else {
            equal = false;
            value[key] = {
                changed: 'removed',
                value: a[key],
            };
        }
    }

    for (const key in b) {
        if (!(key in a)) {
            equal = false;
            value[key] = {
                changed: 'added',
                value: b[key],
            };
        }
    }

    if (equal) {
        return {
            changed: 'equal',
            value: a,
        };
    }
    return {
        changed: 'object change',
        value,
    };
};


/**
 * @param {Object} a
 * @param {Object} b
 * @return {Object}
 */
objectDiff.diffOwnProperties = (a, b) => {
    if (a === b) {
        return {
            changed: 'equal',
            value: a,
        };
    }

    const diff = {};
    let equal = true;
    let keys = Object.keys(a);

    for (let i = 0, length = keys.length; i < length; i++) {
        const key = keys[i];
        if (b.hasOwnProperty(key)) {
            if (a[key] === b[key]) {
                diff[key] = {
                    changed: 'equal',
                    value: a[key],
                };
            } else {
                const typeA = typeof a[key];
                const typeB = typeof b[key];
                if (a[key] && b[key] && (typeA === 'object' || typeA === 'function') && (typeB === 'object' || typeB === 'function')) {
                    const valueDiff = this.diffOwnProperties(a[key], b[key]);
                    if (valueDiff.changed === 'equal') {
                        diff[key] = {
                            changed: 'equal',
                            value: a[key],
                        };
                    } else {
                        equal = false;
                        diff[key] = valueDiff;
                    }
                } else {
                    equal = false;
                    diff[key] = {
                        changed: 'primitive change',
                        removed: a[key],
                        added: b[key],
                    };
                }
            }
        } else {
            equal = false;
            diff[key] = {
                changed: 'removed',
                value: a[key],
            };
        }
    }

    keys = Object.keys(b);

    for (let i = 0, length = keys.length; i < length; i++) {
        const key = keys[i];
        if (!a.hasOwnProperty(key)) {
            equal = false;
            diff[key] = {
                changed: 'added',
                value: b[key],
            };
        }
    }

    if (equal) {
        return {
            value: a,
            changed: 'equal',
        };
    }
    return {
        changed: 'object change',
        value: diff,
    };
};


(function () {
    /**
     * @param {Object} changes
     * @return {string}
     */
    objectDiff.convertToXMLString = (changes) => {
        const properties = [];

        const diff = changes.value;
        if (changes.changed === 'equal') {
            return inspect(diff);
        }

        for (const key in diff) {
            const changed = diff[key].changed;
            switch (changed) {
                case 'equal':
                    properties.push(`${stringifyObjectKey(escapeHTML(key))}<span>: </span>${inspect(diff[key].value)}`);
                    break;

                case 'removed':
                    properties.push(`<del class="diff">${stringifyObjectKey(escapeHTML(key))}<span>: </span>${inspect(diff[key].value)}</del>`);
                    break;

                case 'added':
                    properties.push(`<ins class="diff">${stringifyObjectKey(escapeHTML(key))}<span>: </span>${inspect(diff[key].value)}</ins>`);
                    break;

                case 'primitive change':
                    const prefix = `${stringifyObjectKey(escapeHTML(key))}<span>: </span>`;
                    properties.push(
                        `<del class="diff diff-key">${prefix}${inspect(diff[key].removed)}</del><span>,</span>\n` +
                        `<ins class="diff diff-key">${prefix}${inspect(diff[key].added)}</ins>`);
                    break;

                case 'object change':
                    properties.push(`${stringifyObjectKey(key)}<span>: </span>${this.convertToXMLString(diff[key])}`);
                    break;
            }
        }

        return `<span>{</span>\n<div class="diff-level">${properties.join('<span>,</span>\n')}\n</div><span>}</span>`;
    };

    /**
     * @param {string} key
     * @return {string}
     */
    const stringifyObjectKey = (key) => /^[a-z0-9_$]*$/i.test(key) ?
            key :
            JSON.stringify(key);

    /**
     * @param {string} string
     * @return {string}
     */
    const escapeHTML = (string) => string.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');

    /**
     * @param {Object} obj
     * @return {string}
     */
    const inspect = (obj) => {
        return _inspect('', obj);

        /**
         * @param {string} accumulator
         * @param {object} obj
         * @see http://jsperf.com/continuation-passing-style/3
         * @return {string}
         */
        function _inspect(accumulator, obj) {
            switch (typeof obj) {
                case 'object':
                    if (!obj) {
                        accumulator += 'null';
                        break;
                    }
                    var keys = Object.keys(obj);
                    var length = keys.length;
                    if (length === 0) {
                        accumulator += '<span>{}</span>';
                    } else {
                        accumulator += '<span>{</span>\n<div class="diff-level">';
                        for (let i = 0; i < length; i++) {
                            const key = keys[i];
                            accumulator = _inspect(`${accumulator + stringifyObjectKey(escapeHTML(key))}<span>: </span>`, obj[key]);
                            if (i < length - 1) {
                                accumulator += '<span>,</span>\n';
                            }
                        }
                        accumulator += '\n</div><span>}</span>';
                    }
                    break;

                case 'string':
                    accumulator += JSON.stringify(escapeHTML(obj));
                    break;

                case 'undefined':
                    accumulator += 'undefined';
                    break;

                default:
                    accumulator += escapeHTML(String(obj));
                    break;
            }
            return accumulator;
        }
    };
}());

objectDiff.filterDiff = function filterDiff(obj) {
    const result = {};
    for (const i in obj) {
        if (obj.hasOwnProperty(i)) {
            if (!_.isObject(obj[i])) {
                result[i] = obj[i];
            }
            if (_.isObject(obj[i]) && obj[i].changed !== 'equal') {
                result[i] = obj[i];
                if (obj[i].value && _.isObject(obj[i].value)) {
                    result[i].value = filterDiff(result[i].value);
                }
            }
        }
    }
    return result;
};
