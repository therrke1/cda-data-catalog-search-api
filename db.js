//const { Pool:PoolNotNative } = require('pg');
const { Pool } = require('pg').native;
const { Pool: StreamPool } = require('pg');
const QueryStream = require('pg-query-stream');

const _ = require('lodash');
const logger = require('./middleware/logger');

const env = process.env.NODE_ENV || 'local';

const config = require('./config.json')[env];

const pgConfig = config.pgConfig;
const pool = new Pool(pgConfig);

let acquireCount = 0;
let connectCount = 0;

const _release = (err, client) => {
    client.release(err);
    acquireCount--;
};

pool.on('error', (err, client) => {
    // if an error is encountered by a client while it sits idle in the pool
    // the pool itself will emit an error event with both the error and
    // the client which emitted the original error
    // this is a rare occurrence but can happen if there is a network partition
    // between your application and the database, the database restarts, etc.
    // and so you might want to handle it and at least log it out
    logger.error('idle client error', err.message, err.stack);
});

pool.on('acquire', (client) => {
    acquireCount++;

    if (acquireCount === pgConfig.max) {
        logger.info('connect count:', connectCount);
        logger.info('acquire count:', acquireCount);
        logger.error('WARNING: number of clients which have been acquired from the pool exceeded MAX');
    }
});

pool.on('connect', () => {
    connectCount++;
});

exports.pool = pool;

const executeQuery = (client, query, values = [], noRelease, returnFullResult) => {
    //logger.info('SQL query:', query);
    //console.info('SQL values:', values);
    //console.time('SQL_execution_time');
    const startTime = Date.now();
    return client.query(query, values)
        .then(result => {
            const timeTook = Date.now() - startTime;
            //console.info("RESULT:", result);
            //console.timeEnd('SQL_execution_time');
            if (!noRelease) {
                _release(null, client);
            }

            // if if query execution took more then 1000ms - send warning
            if (timeTook > 1000) {
                logger.warn(`Query execution took ${timeTook} ms`);
            }

            if (returnFullResult) return result;
            return result.rows;
        })
        .catch(err => {
            _release(err, client);
            throw {
                status: 500,
                message: [err, query, values].toString(),
            };
        });
};

exports.executeQuery = executeQuery;

const setDbUser = (client, user521) => executeQuery(client, `set session "dcs_data_catalog.user_521" = '${user521}';`, [], true)
    .then(() => client);

exports.connect = (user521 = null) => pool.connect()
    .then(client => {
        if (user521 != null) {
            return setDbUser(client, user521);
        }
        return client;
    });

/**
 * streamPool is used only for Streaming implementation by using
 * https://github.com/brianc/node-pg-query-stream
 *
 * since this module only works with the JavaScript client, and does not work with the native bindings.
 * libpq doesn't expose the protocol at a level where a cursor can be manipulated directly
 * that's why we have 2 instance of DB Pool
 *
 */
const streamPool = new StreamPool(pgConfig);
exports.streamConnect = () => streamPool.connect()
    .then(client => client);

//pipe 1,000,000 rows to client without blowing up your memory usage
exports.executeQueryStream = (client, query, values = []) => {
    let first = true;
    //console.info('SQL query:', query);
    //console.info('SQL values:', values);
    //console.time('SQL_execution_time');
    return new Promise((resolve, reject) => {
    //let queryStream = new QueryStream('SELECT * FROM generate_series(0, $1) num', [1000000]);
        const queryStream = new QueryStream(query, values);
        const stream = client.query(queryStream);
        //release the client when the stream is finished
        stream.on('end', () => {
            _release(null, client);
        });
        stream.on('readable', () => {
            if (first) {
                //console.timeEnd('SQL_execution_time');
                first = false;
            }
        });
        resolve(stream);
    });
    //stream.pipe(JSONStream.stringify()).pipe(process.stdout);
};
/**
 * Function to execute batch queries
 * It can be used, for example, when you need to update several items and you don't need to have a chain of DB requests
 * @param  {[client]} pg client for DB
 * @param  {[queriesArray]} array of objects like {query:'some query',values:[array of values for this query]}
 * @return promise that resolves with an array that contains each query result
 */
const executeBatchQueries = exports.executeBatchQueries = (client, queriesArray = [], noRelease) => {
    if (queriesArray.length === 0) {
        throw {
            status: 404,
            message: 'Batch queries array is empty',
        };
    }

    const promiseArray = [];
    _.each(queriesArray, (queryObject) => {
        promiseArray.push(executeQuery(client, queryObject.query, queryObject.values, true));
    });
    return Promise.all(promiseArray)
        .then((data) => {
            if (!noRelease) {
                _release(null, client);
            }
            return data;
        });
};

/**
 * Working with Transactions
 * read more:
 * https://github.com/brianc/node-postgres/wiki/Transactions
 **/

const rollbackTransaction = exports.rollbackTransaction = (err, client) => {
    client.query('ROLLBACK', () =>
    //if there was a problem rolling back the query
    //something is seriously messed up.  Return the error
    //to the done function to close & remove this client from
    //the pool.  If you leave a client in the pool with an unaborted
    //transaction weird, hard to diagnose problems might happen.
        _release(err, client));
};

exports.commitTransaction = (client, queries) => executeBatchQueries(client, queries, true).then((results) => client.query('COMMIT')
    .then(() => {
        _release(null, client);
        return results;
    })
    .catch((err) => {
        _release(err, client);
        throw (err);
    }));

exports.connectTransaction = (user521 = null) => {
    const transactionStart = (client) => client.query('BEGIN').then(() => client).catch(err => rollbackTransaction(err, client));
    return pool.connect()
        .then(client => {
            if (user521 != null) {
                return setDbUser(client, user521);
            }
            return client;
        })
        .then((client) => transactionStart(client));
};
