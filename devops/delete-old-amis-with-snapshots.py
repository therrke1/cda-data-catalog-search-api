#!/usr/bin/env python3.5

### Deletes All Orphaned Snapshots for now - Tagging AMIs and Snapshots required to resolve other issues

import boto3
from datetime import date
import re

RegionName = 'us-east-1'
OwnerStringId = '828766338540'
RetentionTime = 14

ec2conn = boto3.client('ec2', region_name=RegionName)

imageIdsList = []
for image in ec2conn.describe_images(Filters = [{'Name': 'owner-id', 'Values': [OwnerStringId]}])['Images']:
  imageIdsList.append(image['ImageId'])
  creation_date = image['CreationDate'].split('T')[0].split('-')
  split_time = (date.today() - date(int(creation_date[0]), int(creation_date[1]), int(creation_date[2]))).days
  if split_time > RetentionTime:
    print(image['ImageId'] + "- with the name - " + image['Name'] + "  is going to be deregistered")
    print("Split_time is : "+str(split_time))
    ec2conn.deregister_image(ImageId=image['ImageId'])
  else:
    imageIdsList.append(image['ImageId'])

reAmi = re.compile('ami-[^ ]+')
for snapshot in ec2conn.describe_snapshots(Filters = [{'Name': 'owner-id', 'Values': [OwnerStringId]}])['Snapshots']:
  snapshotId = snapshot['SnapshotId']
  snapshotImageId = reAmi.findall(snapshot['Description'])[0]
  if snapshotImageId not in imageIdsList:
    ec2conn.delete_snapshot(SnapshotId=snapshotId)
