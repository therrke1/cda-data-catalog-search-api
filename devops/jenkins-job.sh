#!/bin/bash

set -e

TODAY=$(date)
BASE_AMI_ID=$1
INSTANCE_TYPE=$2
INSTANCE_IAM_ROLE=$3
INSTANCE_AUTO_SCALING_IAM_ROLE=$4
ENVIRONMENT=$5
SERVICE_NAME=$6
LAUNCH_CONFIGURATION_NAME=$7
AUTO_SCALING_GROUP_NAME_PREFIX=$8
ASG_MIN_SIZE=$9
ASG_MAX_SIZE=${10}
ASG_DESIRED_CAPACITY=${11}
BUILD_NUMBER=${12}
JOB_NAME=${13}
AWS_REGION=${14}
EC2_SUBNET_ID=${15}
KEYPAIR_NAME=${16}
APPLICATION_NAME=${17}
BRANCH_NAME=${18}
EC2_SECURITY_GROUPS=${19}
EC2_SECURITY_GROUPS=$(sed 's/#/ /g' <<< "$EC2_SECURITY_GROUPS")
GIT_COMMIT=${20}

AUTO_SCALING_GROUP_NAME=$AUTO_SCALING_GROUP_NAME_PREFIX-$ENVIRONMENT
LOG_GROUP_NAME=$APPLICATION_NAME-$ENVIRONMENT
LAUNCH_CONFIGURATION_NAME=$LAUNCH_CONFIGURATION_NAME-$GIT_COMMIT
echo "###########################################################################"
echo "AWS SETUP SETTINGS: "
echo "###########################################################################"

echo "BASE_AMI_ID: $BASE_AMI_ID"
echo "INSTANCE_TYPE: $INSTANCE_TYPE"
echo "INSTANCE_IAM_ROLE: $INSTANCE_IAM_ROLE"
echo "ENVIRONMENT: $ENVIRONMENT"
echo "BUILD_NUMBER: $BUILD_NUMBER"
echo "SERVICE_NAME: $SERVICE_NAME"
echo "JOB_NAME: $JOB_NAME"
echo "LAUNCH_CONFIGURATION_NAME: $LAUNCH_CONFIGURATION_NAME"
echo "INSTANCE_AUTO_SCALING_IAM_ROLE: $INSTANCE_AUTO_SCALING_IAM_ROLE"
echo "AUTO_SCALING_GROUP_NAME: $AUTO_SCALING_GROUP_NAME"
echo "ASG_MIN_SIZE: $ASG_MIN_SIZE"
echo "ASG_MAX_SIZE: $ASG_MAX_SIZE"
echo "ASG_DESIRED_CAPACITY: $ASG_DESIRED_CAPACITY"
echo "AWS_REGION: $AWS_REGION"
echo "EC2_SECURITY_GROUPS: $EC2_SECURITY_GROUPS"
echo "EC2_SUBNET_ID: $EC2_SUBNET_ID"
echo "KEYPAIR_NAME: $KEYPAIR_NAME"
echo "LOG_GROUP_NAME: $LOG_GROUP_NAME"

export AWS_DEFAULT_REGION=$AWS_REGION

echo "###########################################################################"
echo "STEP 1: Spin up EC2 based on AMI: $BASE_AMI_ID"
echo "###########################################################################"

instance_id=$(aws --query='Instances[0].InstanceId' ec2 run-instances\
   --image-id $BASE_AMI_ID\
   --count 1\
   --instance-type $INSTANCE_TYPE\
   --iam-instance-profile $INSTANCE_IAM_ROLE\
   --key-name $KEYPAIR_NAME\
   --security-group-ids $EC2_SECURITY_GROUPS\
   --subnet-id $EC2_SUBNET_ID)
instance_id=$(sed -e 's/^"//' -e 's/"$//' <<<"$instance_id")

echo "STEP 1 is DONE!"

echo "###########################################################################"
echo "STEP 2: Wait until the new EC2 instance is up and running Instance ID: $instance_id"
echo "###########################################################################"

aws ec2 wait instance-status-ok --instance-ids $instance_id
instance_status=$(aws ec2 describe-instance-status --instance-ids $instance_id)
echo "${instance_status}"

echo "STEP 2 is DONE!"

echo "###########################################################################"
echo "STEP 3: Let set tags to the instance"
echo "###########################################################################"

aws ec2 create-tags --resources $instance_id --tags Key=Name,Value=api-$ENVIRONMENT-$SERVICE_NAME-$BUILD_NUMBER-$GIT_COMMIT \
Key=Build_number,Value=$BUILD_NUMBER \
Key=Build_time,Value="$TODAY"

echo "STEP 3 is DONE!"

echo "###########################################################################"
echo "STEP 4: Install the $SERVICE_NAME"
echo "###########################################################################"

instance_ip=$(aws --query='Reservations[0].Instances[0].PrivateIpAddress' ec2 describe-instances --instance-ids $instance_id)
instance_ip=$(sed -e 's/^"//' -e 's/"$//' <<<"$instance_ip")
echo "${instance_ip}"

ssh -o StrictHostKeyChecking=no \
	-o UserKnownHostsFile=/dev/null \
	-F /dev/null \
	-i ./dcs-share.pem \
    -T ec2-user@$instance_ip "sudo mkdir -p /var/www/api/ && sudo chown ec2-user:ec2-user /var/www && sudo chown ec2-user:ec2-user /var/www/api/"

#use -v flag for verbose logs
echo 'Uploading codebase...'
scp -r \
	-o StrictHostKeyChecking=no \
	-o UserKnownHostsFile=/dev/null \
    -F /dev/null \
	-i ./$KEYPAIR_NAME.pem \
	* ec2-user@$instance_ip:/var/www/api/


echo 'Installing dependencies...'
ssh -o StrictHostKeyChecking=no \
	-o UserKnownHostsFile=/dev/null \
	-F /dev/null \
	-i ./$KEYPAIR_NAME.pem \
    -T ec2-user@$instance_ip "cd /var/www/api/; \
    						  sudo bash devops/setup-environment.sh $LOG_GROUP_NAME; \
                              cd /var/www/api/; \
                              export https_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"; \
                              export http_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"; \
                              rm -rf node_modules; \
                              npm install; \
                              export https_proxy=""; \
                              export http_proxy="";"

echo 'Installing UI...'
ssh -o StrictHostKeyChecking=no \
	-o UserKnownHostsFile=/dev/null \
	-F /dev/null \
	-i ./$KEYPAIR_NAME.pem \
    -T ec2-user@$instance_ip "cd /var/www/api/ui/; \
                              export https_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"; \
                              export http_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"; \
                              rm -rf node_modules; \
                              npm install; \
                              npm run build; \
                              export https_proxy=""; \
                              export http_proxy="";"

echo 'Configuring application service...'
ssh -o StrictHostKeyChecking=no \
	-o UserKnownHostsFile=/dev/null \
	-F /dev/null \
    -tt \
	-i ./$KEYPAIR_NAME.pem \
    ec2-user@$instance_ip "stty raw -echo; \
    sudo mv -f /var/www/api/devops/${SERVICE_NAME}.service /etc/systemd/system/; \
    sudo systemctl enable $SERVICE_NAME; \
    sudo service $SERVICE_NAME start;"

echo "STEP 4 is DONE!"

echo "###########################################################################"
echo "STEP 5: Lets create AMI based on the just created EC2 instance id: $instance_id"
echo "###########################################################################"
### see more: http://docs.aws.amazon.com/cli/latest/reference/ec2/create-image.html

ami_id=$(aws --query='ImageId' ec2 create-image --instance-id $instance_id --name "$JOB_NAME-v$BUILD_NUMBER-$GIT_COMMIT" --description "An AMI for $SERVICE_NAME server $ENVIRONMENT environment with GIT-COMMIT id $GIT_COMMIT")
ami_id=$(sed -e 's/^"//' -e 's/"$//' <<<"$ami_id")

echo "STEP 5 is DONE! AMI id is: ${ami_id}"

echo "###########################################################################"
echo "STEP 6: Wait until the new AMI is available, AMI ID: $ami_id"
echo "###########################################################################"

### see more: http://docs.aws.amazon.com/cli/latest/reference/ec2/wait/image-available.html
aws ec2 wait image-available --image-ids $ami_id

echo "STEP 6 is DONE!"

echo "###########################################################################"
echo "STEP 7: Let's terminate instance that was used for AMI: $instance_id"
echo "###########################################################################"

aws ec2 terminate-instances --instance-ids $instance_id

echo "STEP 7 is DONE!"

echo "###########################################################################"
echo "STEP 8: Let's create new launch configuration: $LAUNCH_CONFIGURATION_NAME"
echo "###########################################################################"

autoscaling=$(aws autoscaling create-launch-configuration\
    --launch-configuration-name $LAUNCH_CONFIGURATION_NAME\
    --image-id $ami_id\
    --instance-type $INSTANCE_TYPE\
    --security-groups $EC2_SECURITY_GROUPS\
    --iam-instance-profile $INSTANCE_AUTO_SCALING_IAM_ROLE)

echo "${autoscaling}"

echo "STEP 8 is DONE!"

echo "###########################################################################"
echo "STEP 9: Let's update auto scaling group: $AUTO_SCALING_GROUP_NAME"
echo "###########################################################################"

#http://docs.aws.amazon.com/cli/latest/reference/autoscaling/update-auto-scaling-group.html

aws autoscaling update-auto-scaling-group \
	--auto-scaling-group-name $AUTO_SCALING_GROUP_NAME \
    --launch-configuration-name "$LAUNCH_CONFIGURATION_NAME" \
    --min-size ${ASG_MIN_SIZE} \
    --max-size ${ASG_MAX_SIZE} \
    --desired-capacity ${ASG_DESIRED_CAPACITY}

echo "STEP 9 is DONE!"

echo "###########################################################################"
echo "STEP 10: Let's force refresh auto scaling group $AUTO_SCALING_GROUP_NAME"
echo "###########################################################################"

#https://github.com/AndrewFarley/farley-aws-missing-tools/tree/master/aws-autoscaling-rollout
export AWS_DEFAULT_REGION=$AWS_REGION
devops/aws-autoscaling-rollout.py -a $AUTO_SCALING_GROUP_NAME

echo "###########################################################################"
echo "STEP 11: Delete OLD AMIs older than 14 days and Orphaned AMIs in the account"
echo "###########################################################################"

devops/delete-old-amis-with-snapshots.py

echo "###########################################################################"
echo "ALL STEPS COMPLETED!!!"
echo "###########################################################################"
