#!/bin/bash

#install all required files
set -o errexit

LOG_GROUP_NAME=$1

sudo sed -i -e '$ a\
proxy=http://nibr-proxy.global.nibr.novartis.net:2011' /etc/yum.conf

export https_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"
export http_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"

## Setting Ulimit
echo '*               soft    nofile          1048576' | sudo tee --append /etc/security/limits.conf
echo '*               hard    nofile          1048576' | sudo tee --append /etc/security/limits.conf


#update system & install system required libraries
yum update -y &&\
yum -y install unzip sudo which libaio epel-release gcc-c++ make git cyrus-sasl-devel &&\
yum clean -y all

wget --no-check-certificate https://nodejs.org/dist/v8.2.1/node-v8.2.1-linux-x64.tar.xz -e use_proxy=yes -e https_proxy="http://nibr-proxy.global.nibr.novartis.net:2011"
tar --strip-components 1 -xJf node-v8.2.1-linux-x64.tar.xz -C /usr/local

sudo bash -c 'cat << EOF > /etc/profile
export PATH=$PATH:/usr/local/bin
EOF'
source /etc/profile

#install PostgreSQL dependencies
yum -y install postgresql-devel libpqxx-devel

npm install -g pm2 node-gyp

#install awslogs agent
cd ~
curl https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py -O

rm -f ./awslogs.conf

echo "[general]" >> ./awslogs.conf
echo "state_file = /var/awslogs/state/agent-state" >> ./awslogs.conf
echo "[/var/log/messages]" >> ./awslogs.conf
echo "datetime_format =  %b %d %H:%M:%S" >> ./awslogs.conf
echo "file = /var/log/messages" >> ./awslogs.conf
echo "buffer_duration = 5000" >> ./awslogs.conf
echo "log_stream_name = {hostname}" >> ./awslogs.conf
echo "initial_position = end_of_file" >> ./awslogs.conf
echo "log_group_name = $LOG_GROUP_NAME" >> ./awslogs.conf

python ./awslogs-agent-setup.py --region us-east-1 --non-interactive --configfile=./awslogs.conf


#install custom metrics cloudwatch client
yum install perl-Switch perl-DateTime perl-Sys-Syslog perl-LWP-Protocol-https perl-Digest-SHA -y
yum install zip unzip -y

curl http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip -O
unzip CloudWatchMonitoringScripts-1.2.1.zip
rm CloudWatchMonitoringScripts-1.2.1.zip
cd aws-scripts-mon

#crontab -l > aws-metrics-cron
echo "* * * * * /root/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail" >> aws-metrics-cron
crontab aws-metrics-cron
rm aws-metrics-cron
echo "Set crontab:"
crontab -l
systemctl enable crond


export https_proxy=""
export http_proxy=""
