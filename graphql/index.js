const graphql = require('graphql');

const schema = new graphql.GraphQLSchema({
    query: new graphql.GraphQLObjectType({
        name: 'RootQueryType',
        fields: {
            nibr521: {
                type: graphql.GraphQLString,
                resolve(parentValue, args, request) {
                    return request.headers.nibr521;
                },
            },
        },
    }),
});

exports.schema = schema;
