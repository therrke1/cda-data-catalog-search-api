const env = process.env.NODE_ENV || 'local';

const { createErrorObject } = require('../services/common/utils');
const logger = require('./logger');
const config = require('../config.json')[env];

const devUser = config.devUser;

exports.authenticateUser = (req, res, next) => {
    let user;
    if (env === 'local' && devUser) {
        user = devUser;
        req.headers.nibr521 = user.toUpperCase();
        logger.info(`DEV USER ${user}`);
    } else {
        user = req.header('NIBR521');
    }
    if (!user) {
        next(createErrorObject(401));
    }
    next();
};
