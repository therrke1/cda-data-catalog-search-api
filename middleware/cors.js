exports.enable = function (req, res, next) {
    if (req.get('Origin')) {
        res.set({
            'Access-Control-Allow-Origin': req.get('Origin'),
            'Access-Control-Allow-Credentials': true,
        });
    } else {
        res.set('Access-Control-Allow-Origin', '*');
    }

    res.set({
        'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT, OPTION',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, NIBR521, appApiKey, Accept',
        apiVersion: '1.0.0',
        'Access-Control-Allow-Max-Age': 3600,
    });

    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    }

    return next();
};
