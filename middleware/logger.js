const winston = require('winston');

/**
 * Module creates logger object in Node global scope.
 * Usage example: logger.warn('Some warning')
 *
 * Log levels:
 * - silly
 * - debug
 * - verbose
 * - info
 * - warn
 * - error
 */

const winstonLogger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)({
            timestamp: false,
            level: (process.env.NODE_ENV === 'dev' || process.env.NODE_ENV === 'local') ? 'verbose' : 'warn',
            silent: false,
            colorized: true,
        }),
    ],
});

const stringify = function () {
    const args = Array.prototype.slice.call(arguments);

    return args.map((arg, i) => arg[i]).map(arg => {
        if (typeof arg === 'object') {
            return JSON.stringify(arg).trim();
        }

        return arg;
    }).join(' ');
};

const logger = {
    silly() {
        winstonLogger.silly(stringify(arguments));
    },
    debug() {
        winstonLogger.debug(stringify(arguments));
    },
    verbose() {
        winstonLogger.verbose(stringify(arguments));
    },
    info() {
        winstonLogger.info(stringify(arguments));
    },
    warn() {
        winstonLogger.warn(stringify(arguments));
    },
    error() {
        winstonLogger.error(stringify(arguments));
    },
};

module.exports = logger;
