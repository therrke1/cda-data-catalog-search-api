const logger = require('./logger');

module.exports = (req, res, next) => {
    const start = Date.now();

    res.on('finish', () => {
        const responseTime = Date.now() - start;

        logger.info(`User: ${req.get('nibr521')} ${req.method} ${req.url} Status: ${res.statusCode} ${responseTime}ms`);
    });
    next();
};
