
exports.insertDataItemQuery = `
INSERT INTO data_catalog.data_item
            (
                        "data_item_key",
                        "name",
                        "owner_key",
                        "responsible_organization_key",
                        "source_id",
                        "data_format_id",
                        "container_type_id",
                        "igm_classification_id",
                        "privacy_category_id",
                        "data_domain_id",
                        "created_by",
                        "updated_by",
                        "created_on",
                        "updated_on"
            )
            VALUES
            (
                        $1,
                        $2,
                        $3,
                        $4,
                        (
                                 SELECT   source_id
                                 FROM     data_catalog.source
                                 WHERE    code = $5
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   data_format_id
                                 FROM     data_catalog.data_format
                                 WHERE    code = $6
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   container_type_id
                                 FROM     data_catalog.container_type
                                 WHERE    code = $7
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   igm_classification_id
                                 FROM     data_catalog.igm_classification
                                 WHERE    code = $8
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   privacy_category_id
                                 FROM     data_catalog.privacy_category
                                 WHERE    code = $9
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   data_domain_id
                                 FROM     data_catalog.data_domain
                                 WHERE    code = $10
                                 ORDER BY updated_on DESC limit 1),
                        $11,
                        $12,
                        CURRENT_TIMESTAMP at time zone 'UTC',
                        CURRENT_TIMESTAMP at time zone 'UTC'
            )
            returning *`;

exports.insertDataItemQuerywoUUID = `
INSERT INTO data_catalog.data_item
            (
                        "data_item_key",
                        "name",
                        "owner_key",
                        "responsible_organization_key",
                        "source_id",
                        "data_format_id",
                        "container_type_id",
                        "igm_classification_id",
                        "privacy_category_id",
                        "data_domain_id",
                        "created_by",
                        "updated_by",
                        "created_on",
                        "updated_on"
            )
            VALUES
            (
                        uuid_generate_v4(),
                        $1,
                        $2,
                        $3,
                        (
                                 SELECT   source_id
                                 FROM     data_catalog.source
                                 WHERE    code = $4
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   data_format_id
                                 FROM     data_catalog.data_format
                                 WHERE    code = $5
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   container_type_id
                                 FROM     data_catalog.container_type
                                 WHERE    code = $6
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   igm_classification_id
                                 FROM     data_catalog.igm_classification
                                 WHERE    code = $7
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   privacy_category_id
                                 FROM     data_catalog.privacy_category
                                 WHERE    code = $8
                                 ORDER BY updated_on DESC limit 1),
                        (
                                 SELECT   data_domain_id
                                 FROM     data_catalog.data_domain
                                 WHERE    code = $9
                                 ORDER BY updated_on DESC limit 1),
                        $10,
                        $11,
                        CURRENT_TIMESTAMP at time zone 'UTC',
                        CURRENT_TIMESTAMP at time zone 'UTC'
            )
            returning *`;

exports.selectDataItemQuery = `
SELECT *
FROM   data_catalog.data_item
WHERE  data_item_key = $1
LIMIT  1`;
