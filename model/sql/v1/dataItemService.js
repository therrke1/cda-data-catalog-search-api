exports.dataItemSelectQuery = (dataItemKey) => `
SELECT di.data_item_id                 AS "dataCatalogId",
       di.data_item_key                AS id,
       di.NAME,
       ic.code                         AS "igmClassificationCode",
       di.responsible_organization_key AS "responsibleOrganizationCode",
       s.code                          AS "sourceCode",
       df.code                         AS "dataFormatCode",
       pc.code                         AS "privacyCategoryCode",
       dd.code                         AS "domainCode",
       di.owner_key                    AS "owner521",
       di.additional_information       AS "metadata",
       ct.code                         AS "subType"
FROM   data_item di,
       source s,
       data_format df,
       container_type ct,
       igm_classification ic,
       privacy_category pc,
       data_domain dd
WHERE  data_item_key = '${dataItemKey}'
       AND di.source_id = s.source_id
       AND di.data_format_id = df.data_format_id
       AND di.container_type_id = ct.container_type_id
       AND di.igm_classification_id = ic.igm_classification_id
       AND di.privacy_category_id = pc.privacy_category_id
       AND di.data_domain_id = dd.data_domain_id`;


exports.dataItemDeleteQuery = (dataItemKey) => `
DELETE
FROM   data_item di
WHERE  data_item_key = '${dataItemKey}' returning di.data_item_id as "dataCatalogId",
       (
              SELECT ct.code
              FROM   container_type ct
              WHERE  di.container_type_id = ct.container_type_id) AS "subType"`;


exports.dataItemPutsQuery = `
UPDATE data_item AS di
SET    NAME =
       CASE
              WHEN $2::text IS NOT NULL THEN $2::text
              ELSE di.NAME
       END,
              responsible_organization_key =
       CASE
              WHEN $3::text IS NOT NULL THEN $3::text
              ELSE di.responsible_organization_key
       END,
              igm_classification_id =
       CASE
              WHEN $4::text IS NOT NULL THEN
                     (
                            SELECT ic.igm_classification_id
                            FROM   igm_classification ic
                            WHERE  ic.code = $4::text)
              ELSE di.igm_classification_id
       END,
              source_id =
       CASE
              WHEN $5::text IS NOT NULL THEN
                     (
                            SELECT s.source_id
                            FROM   source s
                            WHERE  s.code = $5::text)
              ELSE di.source_id
       END,
              data_format_id =
       CASE
              WHEN $6::text IS NOT NULL THEN
                     (
                            SELECT df.data_format_id
                            FROM   data_format df
                            WHERE  df.code = $6::text)
              ELSE di.data_format_id
       END,
              privacy_category_id =
       CASE
              WHEN $7::text IS NOT NULL THEN
                     (
                            SELECT pc.privacy_category_id
                            FROM   privacy_category pc
                            WHERE  pc.code = $7::text)
              ELSE di.privacy_category_id
       END,
              data_domain_id =
       CASE
              WHEN $8::text IS NOT NULL THEN
                     (
                            SELECT dd.data_domain_id
                            FROM   data_domain dd
                            WHERE  dd.code = $8::text)
              ELSE di.data_domain_id
       END
FROM   source s,
       data_format df,
       container_type ct,
       igm_classification ic,
       privacy_category pc,
       data_domain dd
WHERE  data_item_key = $1
RETURNING
       di.data_item_key                AS "dataItemKey",
       di.NAME                         AS "name",
       ic.code                         AS "igmClassificationCode",
       di.responsible_organization_key AS "responsibleOrganizationCode",
       s.code                          AS "sourceCode",
       df.code                         AS "dataFormatCode",
       pc.code                         AS "privacyCategoryCode",
       dd.code                         AS "domainCode",
       di.owner_key                    AS "owner521",
       di.additional_information       AS "metadata",
       ct.code                         AS "subType"
`;

exports.dataItemBulkGet = `
SELECT di.data_item_key                AS "dataItemKey",
       di.NAME                         AS "name",
       ic.code                         AS "igmClassificationCode",
       di.responsible_organization_key AS "responsibleOrganizationCode",
       s.code                          AS "sourceCode",
       df.code                         AS "dataFormatCode",
       pc.code                         AS "privacyCategoryCode",
       dd.code                         AS "domainCode",
       di.owner_key                    AS "owner521",
       di.additional_information       AS "metadata",
       ct.code                         AS "subType"
FROM   data_item di,
       source s,
       data_format df,
       container_type ct,
       igm_classification ic,
       privacy_category pc,
       data_domain dd
WHERE  di.data_item_key = ANY($1::text[])
`;


exports.dataItemBulkPost = `
UPDATE data_item AS di
SET           responsible_organization_key =
       CASE
              WHEN $2::text IS NOT NULL THEN $2::text
              ELSE di.responsible_organization_key
       END,
              igm_classification_id =
       CASE
              WHEN $3::text IS NOT NULL THEN
                     (
                            SELECT ic.igm_classification_id
                            FROM   igm_classification ic
                            WHERE  ic.code = $3::text)
              ELSE di.igm_classification_id
       END,
              source_id =
       CASE
              WHEN $4::text IS NOT NULL THEN
                     (
                            SELECT s.source_id
                            FROM   source s
                            WHERE  s.code = $4::text)
              ELSE di.source_id
       END,
              data_format_id =
       CASE
              WHEN $5::text IS NOT NULL THEN
                     (
                            SELECT df.data_format_id
                            FROM   data_format df
                            WHERE  df.code = $5::text)
              ELSE di.data_format_id
       END,
              privacy_category_id =
       CASE
              WHEN $6::text IS NOT NULL THEN
                     (
                            SELECT pc.privacy_category_id
                            FROM   privacy_category pc
                            WHERE  pc.code = $6::text)
              ELSE di.privacy_category_id
       END,
              data_domain_id =
       CASE
              WHEN $7::text IS NOT NULL THEN
                     (
                            SELECT dd.data_domain_id
                            FROM   data_domain dd
                            WHERE  dd.code = $7::text)
              ELSE di.data_domain_id
       END
FROM   source s,
       data_format df,
       container_type ct,
       igm_classification ic,
       privacy_category pc,
       data_domain dd
WHERE  data_item_key = ANY($1::text[])
RETURNING
       di.data_item_key                AS "dataItemKey",
       di.NAME                         AS "name",
       ic.code                         AS "igmClassificationCode",
       di.responsible_organization_key AS "responsibleOrganizationCode",
       s.code                          AS "sourceCode",
       df.code                         AS "dataFormatCode",
       pc.code                         AS "privacyCategoryCode",
       dd.code                         AS "domainCode",
       di.owner_key                    AS "owner521",
       di.additional_information       AS "metadata",
       ct.code                         AS "subType"
`;
