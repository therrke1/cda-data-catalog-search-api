exports.databaseItemSelectQuery = (dataItemId) => `
SELECT database_type AS "databaseType",
       version,
       connection_string AS "connectionString",
       object_name       AS "objectName",
       data_schema       AS "schema",
       created_by        AS "createdBy",
       created_on        AS "createdOn"
FROM   data_item_database
WHERE  data_item_id = ${dataItemId}
ORDER  BY version DESC
LIMIT  1
`;

exports.databaseItemDeleteQuery = (dataItemId) => `
DELETE
FROM   data_item_database
WHERE  data_item_id = ${dataItemId}`;

exports.insertDatabaseItemQuery = `
INSERT INTO data_catalog.data_item_database
            (
                        "data_item_id",
                        "database_type",
                        "data_schema",
                        "connection_string",
                        "object_name",
                        "created_by",
                        "updated_by",
                        "created_on",
                        "updated_on"
            )
            VALUES
            (
                        $1,
                        $2,
                        $3,
                        $4,
                        $5,
                        $6,
                        $7,
                        CURRENT_TIMESTAMP at time zone 'UTC',
                        CURRENT_TIMESTAMP at time zone 'UTC'
            )
            RETURNING *`;

exports.dataItemDatabaseSelectQueryVersion = `
SELECT database_type     AS "databaseType",
       version           AS "version",
       data_schema       AS "schema",
       connection_string AS "connectionString",
       object_name       AS "objectName",
       source_as_of_date AS "sourceAsOfDate",
       created_by        AS "createdBy",
       created_on        AS "createdOn"
FROM   data_item_file
WHERE  data_item_id = (SELECT data_item_id
                       FROM   data_item
                       WHERE  data_item_key = $1)
       AND ( version = $2
              OR $2 IS NULL )
ORDER  BY version DESC
LIMIT  1
`;

exports.dataItemDatabaseGetAllVersions = `
SELECT database_type     AS "databaseType",
       version           AS "version",
       data_schema       AS "schema",
       connection_string AS "connectionString",
       object_name       AS "objectName",
       source_as_of_date AS "sourceAsOfDate",
       created_by        AS "createdBy",
       created_on        AS "createdOn"
FROM   data_item_file
WHERE  data_item_id = (SELECT data_item_id
                       FROM   data_item
                       WHERE  data_item_key = $1)
`;
