
exports.insertFileDataItemQuery = `
INSERT INTO data_catalog.data_item_file
            (
                        "data_item_id",
                        "storage_url",
                        "data_schema",
                        "file_hash",
                        "file_version",
                        "created_by",
                        "updated_by",
                        "source_as_of_date",
                        "created_on",
                        "updated_on"
            )
            VALUES
            (
                        $1,
                        $2,
                        $3,
                        $4,
                        $5,
                        $6,
                        $7,
                        CURRENT_TIMESTAMP at time zone 'UTC',
                        CURRENT_TIMESTAMP at time zone 'UTC',
                        CURRENT_TIMESTAMP at time zone 'UTC'
            )
            returning *`;

exports.dataItemFileSelectQuery = (dataItemId) => `
SELECT storage_url       AS "storageUrl",
       version           AS "version",
       data_schema       AS "schema",
       file_hash         AS "hash",
       source_as_of_date AS "sourceAsOfDate",
       created_by        AS "createdBy",
       created_on        AS "createdOn"
FROM   data_item_file
WHERE  data_item_id = ${dataItemId}
ORDER  BY version DESC
LIMIT  1
`;

exports.dataItemFileSelectQueryVersion = `
SELECT storage_url       AS "storageUrl",
       version           AS "version",
       data_schema       AS "schema",
       file_hash         AS "hash",
       source_as_of_date AS "sourceAsOfDate",
       created_by        AS "createdBy",
       created_on        AS "createdOn"
FROM   data_item_file
WHERE  data_item_id = (SELECT data_item_id
                       FROM   data_item
                       WHERE  data_item_key = $1)
       AND ( version = $2
              OR $2 IS NULL )
ORDER  BY version DESC
LIMIT  1
`;

exports.dataItemFileGetAllVersions = `
SELECT storage_url       AS "storageUrl",
       version           AS "version",
       data_schema       AS "schema",
       file_hash         AS "hash",
       source_as_of_date AS "sourceAsOfDate",
       created_by        AS "createdBy",
       created_on        AS "createdOn"
FROM   data_item_file
WHERE  data_item_id = (SELECT data_item_id
                       FROM   data_item
                       WHERE  data_item_key = $1)
`;

exports.dataItemFileDeleteQuery = (dataItemId) => `
DELETE
FROM   data_item_file
WHERE  data_item_id = ${dataItemId}
RETURNING *`;
