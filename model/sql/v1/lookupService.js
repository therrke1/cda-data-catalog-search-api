exports.getSelectLookupQuery = (lookupType) => `
SELECT * 
FROM   ${lookupType}`;

exports.getDeleteLookupQuery = (lookupType) => `
DELETE 
FROM   ${lookupType} 
WHERE  ${lookupType}_id = $1`;

exports.getInsertLookupQuery = (lookupType) => `
INSERT INTO ${lookupType} 
            ( 
                        ${lookupType}_id, 
                        code, 
                        NAME, 
                        created_by, 
                        created_on, 
                        updated_by, 
                        updated_on 
            ) 
            VALUES 
            ( 
                        nextval('seq_object_id'), 
                        $1, 
                        $2, 
                        $3, 
                        CURRENT_TIMESTAMP, 
                        $3, 
                        CURRENT_TIMESTAMP 
            )`;

exports.getUpdateLookupQuery = (lookupType) => `
UPDATE ${lookupType} 
SET    code = $2, 
       NAME = $3, 
       updated_on = CURRENT_TIMESTAMP, 
       updated_by = $4 
WHERE  ${lookupType}_id = $1`;

exports.getInsertSourceLookupQuery = () => `
INSERT INTO source 
            (source_id, 
             code, 
             NAME, 
             description, 
             contact, 
             url, 
             created_by, 
             created_on, 
             updated_by, 
             updated_on) 
VALUES      ( Nextval('seq_object_id'), 
              $1, 
              $2, 
              $3, 
              $5, 
              $6, 
              CURRENT_TIMESTAMP, 
              $6, 
              CURRENT_TIMESTAMP )`;

exports.getUpdateSourceLookupQuery = () => `
UPDATE source 
SET    code = $2, 
       NAME = $3, 
       description = $4, 
       contact = $5, 
       url = $6 
WHERE  source_id = $1`;
