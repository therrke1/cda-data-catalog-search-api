exports.queryCatalog = `
SELECT di.data_item_id                 AS "dataCatalogId",
       di.data_item_key                AS id,
       di.name                         AS "name",
       ic.code                         AS "igmClassificationCode",
       di.responsible_organization_key AS "responsibleOrganizationCode",
       s.code                          AS "sourceCode",
       df.code                         AS "dataFormatCode",
       pc.code                         AS "privacyCategoryCode",
       dd.code                         AS "domainCode",
       di.owner_key                    AS "owner521",
       di.additional_information       AS "metadata",
       ct.code                         AS "subType"
FROM
( SELECT *
FROM
       data_item
WHERE  ( data_item.responsible_organization_key = ANY($1::text[])
          OR $1 IS NULL )
       AND data_domain_id IN (
       SELECT data_domain.data_domain_id
                              FROM   data_domain
                              WHERE  ( data_domain.code = ANY($2::text[])
                              OR $2 IS NULL ))
       AND igm_classification_id IN (
       SELECT igm_classification.igm_classification_id
                                     FROM   igm_classification
                                     WHERE  ( igm_classification.code = ANY($3::text[])
                                     OR $3 IS NULL ))
       AND privacy_category_id IN (
       SELECT privacy_category.privacy_category_id
                                   FROM   privacy_category
                                   WHERE  ( privacy_category.code = ANY($4::text[])
                                   OR $4 IS NULL ))
       AND (data_item_id IN (
       SELECT data_item_entity_type.data_item_id
                            FROM   data_item_entity_type
                            WHERE  ( data_item_entity_type.entity_type_key = ANY($5::text[])
                            OR $5 IS NULL )) OR NULL IS NULL)
       AND (data_item_id IN (
       SELECT data_item_entity_instance.data_item_id
                            FROM   data_item_entity_instance
                            WHERE  ( data_item_entity_instance.entity_instance_key = ANY($6::text[])
                            OR $6 IS NULL )) OR NULL IS NULL) ) as di,
  source s,
  data_format df,
  container_type ct,
  igm_classification ic,
  privacy_category pc,
  data_domain dd
WHERE
  di.source_id = s.source_id
  AND di.data_format_id = df.data_format_id
  AND di.container_type_id = ct.container_type_id
  AND di.igm_classification_id = ic.igm_classification_id
  AND di.privacy_category_id = pc.privacy_category_id
  AND di.data_domain_id = dd.data_domain_id;`;
