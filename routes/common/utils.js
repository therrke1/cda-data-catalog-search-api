const util = require('util');
const Form = require('reformed').Form;

const createErrorObject = (status, message) => {
    const errorMessages = {
        4100: 'Route Validation Errors',
        4101: 'Form Validation Errors',
        4102: 'Special Business Logic Errors',
        4103: 'AWS File Service Errors',
    };
    if (!message) {
        message = errorMessages[status] || '';
    }
    const err = new Error(message);
    err.status = status;
    return err;
};

exports.createErrorObject = createErrorObject;

exports.routeValidation = (req, res, next) => new Promise(resolve => {
    req.getValidationResult().then((result) => {
        if (!result.isEmpty()) {
            const errorMsg = {
                errorType: 'Validation Error',
                error: result.array(),
            };
            res.status(400).json(errorMsg);
            throw createErrorObject(4100, JSON.stringify(errorMsg));
        }
        resolve();
    });
});

exports.formValidation = (req, res, next) => new Promise(resolve => {
    const form = new Form(req.ValidSchema);
    form.parse(req.busboy, (err) => {
        if (err) {
            const errorMsg = {
                errorType: 'Validation Error',
                error: [{
                    key: err.key,
                    msg: err.message,
                }],
            };
            res.status(400).json(errorMsg);
            throw createErrorObject(4101, JSON.stringify(errorMsg));
        }
    });
    resolve();
});

exports.specialErrors = (req, res, msg, next) => new Promise(resolve => {
    const errorMsg = {
        errorType: 'Special Error',
        error: [{
            msg,
        }],
    };
    res.status(400).json(errorMsg);
    throw createErrorObject(4101, JSON.stringify(errorMsg));
});

