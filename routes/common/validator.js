exports.expressValidatorOptions = {
    errorFormatter(param, msg, value) {
        const namespace = param.split('.'),
            root = namespace.shift();
        let formParam = root;

        while (namespace.length) {
            formParam += `[${namespace.shift()}]`;
        }
        return {
            param: formParam,
            msg,
            value,
        };
    },
    customValidators: {
        isArray(value) {
            return Array.isArray(value);
        },
        gte(param, num) {
            return param >= num;
        },
    },
    customSanitizers: {
        toArray(value) {
            const newValue = value.split(',');
            return newValue;
        },
        toLowercase(value) {
            const newValue = value.toLowerCase();
            return newValue;
        },
    },
};

exports.IsJsonString = (key, val) => {
    try {
        JSON.parse(val);
    } catch (e) {
        return false;
    }
    return true;
};
