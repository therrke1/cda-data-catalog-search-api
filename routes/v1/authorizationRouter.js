const express = require('express');
const authorizationService = require('../../services/v1/authorizationService');

const router = express.Router();

router.get('/authorization/:nibr521', (req, res, next) => {
    authorizationService.getUserPermissions(req.params.nibr521)
        .then(data => res.status(200).json(data))
        .catch(next);
});

module.exports = router;
