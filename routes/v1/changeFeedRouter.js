const express = require('express');
const changeFeedService = require('../../services/v1/changeFeedService');
const utils = require('../common/utils');
const changeFeedRouterValid = require('./validators/changeFeedRouterValidator');

const router = express.Router();

router.get('/changeFeed', (req, res, next) => {
    req.sanitize('entityTypes').toArray();
    req.sanitize('entityInstances').toArray();
    req.sanitize('parentsFlag').toLowercase();

    req.ValidSchema = changeFeedRouterValid.changeFeed_Get;
    req.check(req.ValidSchema);

    req.checkQuery('endDate', 'EndDate should be Before now').isBefore(new Date().toString());
    req.checkQuery('endDate', 'EndDate should be After the StartDate').isAfter(req.query.startDate);

    utils.routeValidation(req, res).then(() => {
        res.status(200).json({});
    }).catch(next);
});

module.exports = router;
