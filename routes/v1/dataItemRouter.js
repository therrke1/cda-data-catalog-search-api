const express = require('express');
const dataItemService = require('../../services/v1/dataItemService');
const utils = require('../common/utils');
const dataItemRouterValid = require('./validators/dataItemRouterValidator');

const router = express.Router();

router.delete('/dataItems/:id', (req, res, next) => {
    req.ValidSchema = dataItemRouterValid.dataItems_Id_Delete;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        dataItemService.deleteDataItem(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.get('/dataItems/:id', (req, res, next) => {
    req.ValidSchema = dataItemRouterValid.dataItems_Id_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        dataItemService.getDataItem(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.put('/dataItems/:id', (req, res, next) => {
    req.ValidSchema = dataItemRouterValid.dataItems_Id_Put;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        dataItemService.putDataItem(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.post('/dataItems', (req, res, next) => {
    req.ValidSchema = dataItemRouterValid.dataItems_Post;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        dataItemService.bulkGetItems(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.put('/dataItems', (req, res, next) => {
    req.ValidSchema = dataItemRouterValid.dataItems_Put;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        dataItemService.bulkUpdateItems(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.get('/dataItems/:id/entityTags', (req, res, next) => {
    req.ValidSchema = dataItemRouterValid.dataItems_Id_Entitytags_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        res.status(200).json({});
    }).catch(next);
});

router.post('/dataItems/:id/entityTags', (req, res, next) => {
    req.ValidSchema = dataItemRouterValid.dataItems_Id_Entitytags_Post;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        res.status(200).json({});
    }).catch(next);
});

router.put('/dataItems/:id/entityTags', (req, res, next) => {
    req.ValidSchema = dataItemRouterValid.dataItems_Id_Entitytags_Put;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        res.status(200).json({});
    }).catch(next);
});

module.exports = router;
