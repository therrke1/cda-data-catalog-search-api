const express = require('express');
const dataItemSetService = require('../../services/v1/dataItemSetService');
const utils = require('../common/utils');
const dataItemSetRouterValid = require('./validators/dataItemSetValidator');

const router = express.Router();

router.get('/dataItemSets/:id', (req, res, next) => {
    req.ValidSchema = dataItemSetRouterValid.dataItemSets_Id_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        res.status(200).json({});
    }).catch(next);
});

router.post('/dataItemSets', (req, res, next) => {
    req.ValidSchema = dataItemSetRouterValid.dataItemSets_Post;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        res.status(200).json({});
    }).catch(next);
});

router.put('/dataItemSets/:id', (req, res, next) => {
    req.ValidSchema = dataItemSetRouterValid.dataItemSets_Id_Put;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        res.status(200).json({});
    }).catch(next);
});

module.exports = router;
