
const express = require('express');
const databaseService = require('../../services/v1/databaseService');
const utils = require('../common/utils');
const databaseRouterValid = require('./validators/databaseRouterValidator');

const router = express.Router();

router.post('/dataItems/database', (req, res, next) => {
    req.ValidSchema = databaseRouterValid.dataItems_Database_Post;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        databaseService.dataItemCreate(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.get('/dataItems/:id/database', (req, res, next) => {
    req.ValidSchema = databaseRouterValid.dataItems_Id_Database_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        databaseService.getDatabaseDetails(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.post('/dataItems/:id/database', (req, res, next) => {
    req.ValidSchema = databaseRouterValid.dataItems_Id_Database_Post;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        databaseService.dataItemUpdate(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.get('/dataItems/:id/database/versions', (req, res, next) => {
    req.ValidSchema = databaseRouterValid.dataItems_Id_Database_Versions_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        databaseService.getDatabaseVersions(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

module.exports = router;
