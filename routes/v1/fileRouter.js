const express = require('express');

const fileService = require('../../services/v1/fileService');
const utils = require('../common/utils');
const fileRouterValid = require('./validators/fileRouterValidator');
const awsServices = require('../../services/common/aws-services');

const router = express.Router();

router.post('/dataItems/file', (req, res, next) => {
    req.ValidSchema = fileRouterValid.dataItems_File_Post;

    utils.formValidation(req, res)
      .then(() => {
          fileService.uploadCreate()(req, res, (err) => {
              if (err) {
                  throw utils.createErrorObject(4103);
              } else {
                  fileService.DataItemCreate(req, res)
                    .then(data => res.status(200).json(data))
                    .catch(next);
              }
          }
      );
      }).catch(next);
});

router.post('/dataItems/:id/file', (req, res, next) => {
    req.ValidSchema = fileRouterValid.dataItems_Id_File_Post;
    req.checkParams('id', 'Invalid Id - UUID Format Required').isUUID();

    utils.formValidation(req, res)
      .then(() => utils.routeValidation(req, res))
      .then(() => {
          fileService.uploadUpdate(req.params.id)(req, res, (err) => {
              if (err) {
                  throw utils.createErrorObject(4103);
              } else {
                  fileService.DataItemUpdate(req, res)
                .then(data => res.status(200).json(data))
                .catch(next);
              }
          }
        );
      }).catch(next);
});

router.get('/dataItems/:id/file', (req, res, next) => {
    req.ValidSchema = fileRouterValid.dataItems_Id_File_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res)
        .then(() => fileService.GetFileDetails(req, res))
        .then(data => {
            if (!data) {
                const readStream = fileService.downloadFile(req, res, data);
                readStream.on('end', () => {
                    // On Data Download End
                });
                readStream.on('error', (err) => {
                    // On Data Download Error
                });
                readStream.pipe(res);
                readStream.on('data', (chunk) => {
                    // On Data being Downloaded
                });
            } else {
                return res.status(404).json({ msg: 'File Not Found' });
            }
        }).catch(next);
});


router.get('/dataItems/:id/file/versions', (req, res, next) => {
    req.ValidSchema = fileRouterValid.dataItems_Id_File_Versions_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        fileService.getFileVersions(req, res).then(data => {
            res.status(200).json(data);
        });
    }).catch(next);
});

module.exports = router;

// TODO Lineage and Tags Attributes Consumption in File Create Route
