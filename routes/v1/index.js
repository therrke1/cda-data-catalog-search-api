const express = require('express');

const lookupRoutes = require('./lookupRouter');
const fileRoutes = require('./fileRouter');
const queryRoutes = require('./queryRouter');
const dataItemRoutes = require('./dataItemRouter');
const changeFeedRoutes = require('./changeFeedRouter');
const databaseRoutes = require('./databaseRouter');
const restfulRoutes = require('./restfulRouter');
const dataItemSetsRoutes = require('./dataItemSetRouter');
const authorizationRoutes = require('./authorizationRouter');

const router = express.Router();

router.use('/', lookupRoutes);
router.use('/', fileRoutes);
router.use('/', queryRoutes);
router.use('/', dataItemRoutes);
router.use('/', changeFeedRoutes);
router.use('/', databaseRoutes);
router.use('/', restfulRoutes);
router.use('/', dataItemSetsRoutes);
router.use('/', authorizationRoutes);

module.exports = router;
