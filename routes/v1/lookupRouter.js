const express = require('express');
const lookupService = require('../../services/v1/lookupService');

const LOOKUP_TYPES = lookupService.LOOKUP_TYPES;

const router = express.Router();

/**
 * domains
 */
router.get('/domains', (req, res, next) => {
    lookupService.getLookups(LOOKUP_TYPES.dataDomain, req.url)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.post('/domains', (req, res, next) => {
    lookupService.saveOrUpdateLookups(req.body.lookups, LOOKUP_TYPES.dataDomain,
        req.headers.nibr521)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.delete('/domains/:id', (req, res, next) => {
    lookupService.deleteLookup(req.params.id, LOOKUP_TYPES.dataDomain,
        req.headers.nibr521)
        .then(() => res.sendStatus(200))
        .catch(next);
});

/**
 * formats
 */
router.get('/formats', (req, res, next) => {
    lookupService.getLookups(LOOKUP_TYPES.dataFormat, req.url)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.post('/formats', (req, res, next) => {
    lookupService.saveOrUpdateLookups(req.body.lookups, LOOKUP_TYPES.dataFormat,
        req.headers.nibr521)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.delete('/formats/:id', (req, res, next) => {
    lookupService.deleteLookup(req.params.id, LOOKUP_TYPES.dataFormat,
        req.headers.nibr521)
        .then(() => res.sendStatus(200))
        .catch(next);
});

/**
 * igmClassifications
 */
router.get('/igmClassifications', (req, res, next) => {
    lookupService.getLookups(LOOKUP_TYPES.igmClassification, req.url)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.post('/igmClassifications', (req, res, next) => {
    lookupService.saveOrUpdateLookups(req.body.lookups,
        LOOKUP_TYPES.igmClassification, req.headers.nibr521)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.delete('/igmClassifications/:id', (req, res, next) => {
    lookupService.deleteLookup(req.params.id, LOOKUP_TYPES.igmClassification,
        req.headers.nibr521)
        .then(() => res.sendStatus(200))
        .catch(next);
});

/**
 * privacyCategories
 */
router.get('/privacyCategories', (req, res, next) => {
    lookupService.getLookups(LOOKUP_TYPES.privacyCategory, req.url)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.post('/privacyCategories', (req, res, next) => {
    lookupService.saveOrUpdateLookups(req.body.lookups,
        LOOKUP_TYPES.privacyCategory, req.headers.nibr521)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.delete('/privacyCategories/:id', (req, res, next) => {
    lookupService.deleteLookup(req.params.id, LOOKUP_TYPES.privacyCategory,
        req.headers.nibr521)
        .then(() => res.sendStatus(200))
        .catch(next);
});

/**
 * sources
 */
router.get('/sources', (req, res, next) => {
    lookupService.getLookups(LOOKUP_TYPES.source, req.url)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.post('/sources', (req, res, next) => {
    lookupService.saveOrUpdateLookups(req.body.lookups,
        LOOKUP_TYPES.source, req.headers.nibr521)
        .then(data => res.status(200).json(data))
        .catch(next);
});

router.delete('/sources/:id', (req, res, next) => {
    lookupService.deleteLookup(req.params.id, LOOKUP_TYPES.source,
        req.headers.nibr521)
        .then((data) => res.sendStatus(200))
        .catch(next);
});

module.exports = router;
