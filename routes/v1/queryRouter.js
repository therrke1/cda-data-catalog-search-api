const express = require('express');
const queryService = require('../../services/v1/queryService');
const utils = require('../common/utils');
const queryRouterValid = require('./validators/queryRouterValidator');

const router = express.Router();

router.get('/query', (req, res, next) => {
    req.sanitizeQuery('responsibleOrganizationCodes').toArray();
    req.sanitizeQuery('domainCodes').toArray();
    req.sanitizeQuery('entityTypes').toArray();
    req.sanitizeQuery('entityInstances').toArray();
    req.sanitizeQuery('igmClassificationCodes').toArray();
    req.sanitizeQuery('privacyCategoryCodes').toArray();

    req.ValidSchema = queryRouterValid.query_Get;
    req.check(req.ValidSchema);

    if (!Object.keys(req.query).length) {
        utils.specialErrors(req, res, 'Atleast One Query Parameter required')
          .catch(() => { });
    } else {
        utils.routeValidation(req, res)
            .then(() => queryService.queryDataCatalog(req, res))
            .then(data => {
                res.status(200).json(data);
            }).catch(next);
    }
});

module.exports = router;
