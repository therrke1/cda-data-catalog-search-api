
const express = require('express');
const restfulService = require('../../services/v1/restfulService');
const utils = require('../common/utils');
const restfulRouterValid = require('./validators/restfulRouterValidator');

const router = express.Router();

router.post('/dataItems/restful', (req, res, next) => {
    req.ValidSchema = restfulRouterValid.dataItems_Restful_Post;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        restfulService.dataItemCreate(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.get('/dataItems/:id/restful', (req, res, next) => {
    req.ValidSchema = restfulRouterValid.dataItems_Id_Restful_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        restfulService.getRestfulDetails(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.post('/dataItems/:id/restful', (req, res, next) => {
    req.ValidSchema = restfulRouterValid.dataItems_Id_Restful_Post;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        restfulService.dataItemUpdate(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

router.get('/dataItems/:id/restful/versions', (req, res, next) => {
    req.ValidSchema = restfulRouterValid.dataItems_Id_Restful_Versions_Get;
    req.check(req.ValidSchema);

    utils.routeValidation(req, res).then(() => {
        restfulService.getRestfulVersions(req, res)
            .then(data => res.status(200).json(data))
            .catch(next);
    }).catch(next);
});

module.exports = router;
