
exports.changeFeed_Get = {
    startDate: {
        in: 'query',
        notEmpty: true,
        isISO8601: {
            errorMessage: 'Query parameter should be in ISO08061 Date format',
        },
    },
    endDate: {
        in: 'query',
        optional: true,
        isISO8601: {
            errorMessage: 'Query parameter should be in ISO08061 Date format',
        },
    },
    entityTypes: {
        in: 'query',
        optional: true,
        isArray: {
            errorMessage: 'Query Parameter should be an Array',
        },
    },
    entityInstances: {
        in: 'query',
        optional: true,
        isArray: {
            errorMessage: 'Query Parameter should be an Array',
        },
    },
    parentsFlag: {
        in: 'query',
        optional: true,
        isBoolean: {
            errorMessage: 'Query Parameter should be Boolean value',
        },
    },
};
