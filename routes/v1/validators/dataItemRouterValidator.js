
exports.dataItems_Id_Delete = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
};

exports.dataItems_Id_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
};

exports.dataItems_Id_Put = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    name: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    igmClassificationCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    responsibleOrganizationCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    sourceCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    dataFormatCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    privacyCategoryCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    domainCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
};

exports.dataItems_Post = {
    ids: {
        in: 'body',
        notEmpty: true,
        isArray: {
            errorMessage: 'Must be an Array',
        },
    },
};

exports.dataItems_Put = {
    ids: {
        in: 'body',
        notEmpty: true,
        isArray: {
            errorMessage: 'Must be an Array',
        },
    },
    igmClassificationCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    responsibleOrganizationCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    sourceCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    dataFormatCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    privacyCategoryCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    domainCode: {
        in: 'body',
        optional: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
};

exports.dataItems_Id_Entitytags_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
};

exports.dataItems_Id_Entitytags_Post = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    entityTypeCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    entityInstances: {
        in: 'body',
        notEmpty: true,
        isArray: {
            errorMessage: 'Must be an Array',
        },
    },
};

exports.dataItems_Id_Entitytags_Put = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    entityTypeCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    entityInstances: {
        in: 'body',
        notEmpty: true,
        isArray: {
            errorMessage: 'Must be an Array',
        },
    },
};
