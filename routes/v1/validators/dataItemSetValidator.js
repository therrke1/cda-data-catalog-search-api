
exports.dataItemSets_Id_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
};

exports.dataItemSets_Post = {
    name: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    ids: {
        in: 'body',
        notEmpty: true,
        isArray: {
            errorMessage: 'Must be an Array',
        },
    },
};

exports.dataItemSets_Id_Put = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    name: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    ids: {
        in: 'body',
        notEmpty: true,
        isArray: {
            errorMessage: 'Must be an Array',
        },
    },
};
