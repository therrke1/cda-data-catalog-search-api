
exports.dataItems_Database_Post = {
    name: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    igmClassificationCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    responsibleOrganizationCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    sourceCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    dataFormatCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    privacyCategoryCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    domainCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    databaseType: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    connectionString: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 1000 }],
            errorMessage: 'Must be in String format',
        },
    },
    objectName: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    schema: {
        in: 'body',
        optional: true,
        isJSON: {
            errorMessage: 'Schema needs to be in JSON Format',
        },
    },
};

exports.dataItems_Id_Database_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    version: {
        in: 'query',
        optional: true,
        isInt: {
            errorMessage: 'Version should be an Integer',
        },
    },
};

exports.dataItems_Id_Database_Post = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    databaseType: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    connectionString: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 1000 }],
            errorMessage: 'Must be in String format',
        },
    },
    objectName: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    schema: {
        in: 'body',
        optional: true,
        isJSON: {
            errorMessage: 'Schema needs to be in JSON Format',
        },
    },
};

exports.dataItems_Id_Database_Versions_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
};
