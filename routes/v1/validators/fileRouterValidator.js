const validatorFunction = require('../../common/validator');

// Form Validator
exports.dataItems_File_Post = {
    schema: {
        rules: [{
            test: validatorFunction.IsJsonString,
            error: 'Invalid Schema - Json Format only allowed',
        }],
    },
    name: {
        required: true,
        rules: [{
            test: /^.{1,100}$/,
            error: 'Name must be 100 characters or less',
        }],
    },
    igmClassificationCode: {
        required: true,
        rules: [{
            test: /^.{1,100}$/,
            error: 'Must be 100 characters or less',
        }],
    },
    responsibleOrganizationCode: {
        required: true,
        rules: [{
            test: /^.{1,100}$/,
            error: 'Must be 100 characters or less',
        }],
    },
    dataInclusionRational: {
        rules: [{
            test: /^.{1,1000}$/,
            error: 'Must be 1000 characters or less',
        }],
    },
    sourceCode: {
        required: true,
        rules: [{
            test: /^.{1,100}$/,
            error: 'Must be 100 characters or less',
        }],
    },
    dataFormatCode: {
        required: true,
        rules: [{
            test: /^.{1,100}$/,
            error: 'Must be 100 characters or less',
        }],
    },
    privacyCategoryCode: {
        required: true,
        rules: [{
            test: /^.{1,100}$/,
            error: 'Must be 100 characters or less',
        }],
    },
    domainCode: {
        required: true,
        rules: [{
            test: /^.{1,100}$/,
            error: 'Must be 100 characters or less',
        }],
    },
    lineage: {
        rules: [{
            test: /^.{1,100}$/,
            error: 'Must be 100 characters or less',
        }],
    },
    tags: {
        rules: [{
            test: /^.{1,100}$/,
            error: 'Must be 100 characters or less',
        }],
    },
};


exports.dataItems_Id_File_Post = {
    schema: {
        rules: [{
            test: validatorFunction.IsJsonString,
            error: 'Invalid Schema - Json Format only allowed',
        }],
    },
};


// Express Validator
exports.dataItems_Id_File_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    version: {
        in: 'query',
        optional: true,
        isInt: {
            errorMessage: 'Version should be an Integer',
        },
    },
};

exports.dataItems_Id_File_Versions_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
};

