
exports.query_Get = {
    responsibleOrganizationCodes: {
        in: 'query',
        optional: true,
        isArray: {
            errorMessage: 'Query Parameter should be an Array',
        },
    },
    domainCodes: {
        in: 'query',
        optional: true,
        isArray: {
            errorMessage: 'Query Parameter should be an Array',
        },
    },
    entityTypes: {
        in: 'query',
        optional: true,
        isArray: {
            errorMessage: 'Query Parameter should be an Array',
        },
    },
    entityInstanceCodes: {
        in: 'query',
        optional: true,
        isArray: {
            errorMessage: 'Query Parameter should be an Array',
        },
    },
    igmClassificationCodes: {
        in: 'query',
        optional: true,
        isArray: {
            errorMessage: 'Query Parameter should be an Array',
        },
    },
    privacyCategoryCodes: {
        in: 'query',
        optional: true,
        isArray: {
            errorMessage: 'Query Parameter should be an Array',
        },
    },
};
