
exports.dataItems_Restful_Post = {
    name: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    igmClassificationCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    responsibleOrganizationCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    sourceCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    dataFormatCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    privacyCategoryCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    domainCode: {
        in: 'body',
        notEmpty: true,
        isLength: {
            options: [{ min: 1, max: 100 }],
            errorMessage: 'Must be between 1 and 100 chars long',
        },
    },
    urlEndpoint: {
        in: 'body',
        notEmpty: true,
        isURL: {
            options: [{ protocols: ['http', 'https', 'ftp'] }],
            errorMessage: 'Valid URL Endpoint Required',
        },
    },
    schema: {
        in: 'body',
        optional: true,
        isJSON: {
            errorMessage: 'Schema needs to be in JSON Format',
        },
    },
};

exports.dataItems_Id_Restful_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    version: {
        in: 'query',
        optional: true,
        isInt: {
            errorMessage: 'Version should be an Integer',
        },
    },
};

exports.dataItems_Id_Restful_Post = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
    urlEndpoint: {
        in: 'body',
        notEmpty: true,
        isURL: {
            options: [{ protocols: ['http', 'https', 'ftp'] }],
            errorMessage: 'Valid URL Endpoint Required',
        },
    },
    schema: {
        in: 'body',
        optional: true,
        isJSON: {
            errorMessage: 'Schema needs to be in JSON Format',
        },
    },
};

exports.dataItems_Id_Restful_Versions_Get = {
    id: {
        in: 'params',
        notEmpty: true,
        isUUID: {
            options: [4],
            errorMessage: 'Parameter should be in the UUID v4 format',
        },
    },
};
