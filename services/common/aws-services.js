
const AWS = require('aws-sdk');
const proxy = require('proxy-agent');

const env = process.env.NODE_ENV || 'local';
const config = require('../../config.json')[env];


const s3config = env !== 'local' ? (
    new AWS.S3({
        params: {
            ServerSideEncryption: 'AES256',
        },
    })
) : (
    new AWS.S3({
        params: {
            ServerSideEncryption: 'AES256',
        },
        httpOptions: {
            agent: proxy('http://nibr-proxy.global.nibr.novartis.net:2011'),
        },
    })
);

exports.s3config = s3config;
