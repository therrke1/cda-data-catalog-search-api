const Kafka = require('node-rdkafka');
const { diff, filterDiff } = require('../../common/objectDiff');
const { getDataItem } = require('../v1/dataItemService');
const _ = require('lodash');

const env = process.env.NODE_ENV || 'local';
const config = require('../../config.json')[env];

//
//Message Formatter
//
function formatMessage(type, dataItemId) {
    return new Promise((resolve, reject) => {
        const message = {
            type,
            dataItemId,
        };
        if (type === 'insert') {
            //get data item details
            getDataItem(dataItemId).then(result => {
                _.extend(message, { data: result.member });
                resolve(message);
            }).catch((e) => {
                console.log(e);
                reject(new Error('Get Data Item Error'));
            });
        } else if (type === 'edit') {
            //
        } else if (type === 'delete') {
            //
        } else {
            //
        }
    });
}
//
//Writes message into a kafka topic
//
exports.write2KafkaTopic = (topic, type, dataItemId) => new Promise((resolve, reject) => {
    formatMessage(type, dataItemId).then(result => {
        const producer = new Kafka.Producer({
            'metadata.broker.list': config['kafka-server'],
            dr_cb: true,
        });
            // Connect to the broker manually
        producer.connect();

            // Wait for the ready event before proceeding
        producer.on('ready', () => {
            try {
                producer.produce(
                        // Topic to send the message to
                        topic,
                        // optionally we can manually specify a partition for the message
                        // this defaults to -1 - which will use librdkafka's default partitioner
                        //(consistent random for keyed messages, random for unkeyed messages)
                        null,
                        // Message to send. Must be a buffer
                        new Buffer(JSON.stringify(result)),
                        // for keyed messages, we also specify the key - note that this field is optional
                        null,
                        // you can send a timestamp here. If your broker version supports it,
                        // it will get added. Otherwise, we default to 0
                        Date.now()
                        // you can send an opaque token here, which gets passed along
                        // to your delivery reports
                    );
                resolve('Successful Write of Kafka Message');
            } catch (err) {
                console.error('A problem occurred when sending Kafka message');
                reject(new Error(`Write Kafka Message ${type} Data Item Error: ${err}`));
            }
        });

            // Any errors we encounter, including connection errors
        producer.on('event.error', (err) => {
            console.error('Error from Kafka producer');
            reject(new Error(`Write Kafka Message ${type} Data Item Error: ${err}`));
        });
    }).catch((e) => {
        console.log(e);
        reject(new Error(`Write Kafka Message ${type} Data Item Error: ${e}`));
    });
});

