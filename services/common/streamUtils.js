
const through = require('through');

/**
 * Utils
 */
const stringify = exports.stringify = (op, sep, cl, indent) => {
    indent = indent || 0;
    if (op === false) {
        op = '';
        sep = '\n';
        cl = '';
    } else if (op == null) {
        op = '[\n';
        sep = '\n,\n';
        cl = '\n]\n';
    }

  //else, what ever you like
    let first = true,
        anyData = false;

    const stream = through((data) => {
        anyData = true;
        try {
            const json = JSON.stringify(data, null, indent);
            if (first) {
                first = false; stream.queue(op + json);
            } else stream.queue(sep + json);
        } catch (err) {
            return stream.emit('error', err);
        }
    },
  (data) => {
      if (!anyData) stream.queue(op);
      stream.queue(cl);
      stream.queue(null);
  });

    return stream;
};
