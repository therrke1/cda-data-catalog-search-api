const _ = require('lodash');
const uuidv4 = require('uuid/v4');

const db = require('../../db.js');

exports.executeQuery = db.executeQuery;
exports.executeBatchQueries = db.executeBatchQueries;

const createErrorObject = (status, message) => {
    const errorMessages = {
        401: 'Application Authentication failed',
        403: 'You do not have sufficient privileges to access this content',
        404: 'Not Found',
    };
    if (!message) {
        message = errorMessages[status] || '';
    }
    const err = new Error(message);
    err.status = status;
    return err;
};
exports.createErrorObject = createErrorObject;

exports.check404 = (data) => {
    if (!data.length) {
        throw createErrorObject(404);
    }
    return data;
};

exports.flattenProperties = (data) => data.map(obj =>
    _.chain(obj.properties || {})
      .merge(obj)
      .omit('properties')
      .value()
  );


exports.getFacets = (data, attributes = []) => {
    const facets = {};
    attributes.forEach(attribute => {
        facets[attribute] = _.chain(data).map(attribute).compact().uniq()
        .orderBy()
        .value();
    });
    return facets;
};

exports.stripStartingIn = (value = '') =>
  (value.startsWith && value.startsWith('in ')) ? value.substr(3) : value;

exports.generateParameters = (arr = [], from = 1) =>
  arr.map((item, i) => `$${i + from}`).join(', ');

exports.validateBodyParams = (...params) => function (req, res, next) {
    for (const param of params) {
        if (!req.body[param]) {
            return res.send(400, {
                error: `${param} is required in JSON body`,
            });
        }
    }
    next();
};

exports.UUIDv4 = () => uuidv4();
