
exports.getUserPermissions = (nibr521) => {
    const ADMIN_USERS = [
        'GRANDER1',
        'SHANTSA1',
        'VERNIIG1',
        'FRISHBA1',
        'THERRKE1',
    ];

    return new Promise((resolve) => {
        const rights = ADMIN_USERS.includes(nibr521.toUpperCase())
            ? ['READ', 'EDIT']
            : ['READ'];

        resolve(rights);
    });
};
