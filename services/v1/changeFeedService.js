
const _ = require('lodash');

const dbPool = require('../../db');
const utils = require('../common/utils');
const streamUtils = require('../common/streamUtils');
//const logger = require('../middleware/logger'); if you want to use logger uncomment - these will show up in your cloudwatch log group if you have set this up
const commonSQL = require('../../model/sql/v1/common');
const changeFeedServiceSQL = require('../../model/sql/v1/changeFeedService');

const env = process.env.NODE_ENV || 'local';
const config = require('../../config.json')[env];

