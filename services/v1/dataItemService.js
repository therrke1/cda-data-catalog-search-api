
const _ = require('lodash');

const dbPool = require('../../db');
const utils = require('../common/utils');
const streamUtils = require('../common/streamUtils');
const awsServices = require('../common/aws-services');
//const logger = require('../middleware/logger'); if you want to use logger uncomment - these will show up in your cloudwatch log group if you have set this up
const commonSQL = require('../../model/sql/v1/common');
const dataItemServiceSQL = require('../../model/sql/v1/dataItemService');
const fileServiceSQL = require('../../model/sql/v1/fileService');
const databaseServiceSQL = require('../../model/sql/v1/databaseService');
const restfulServiceSQL = require('../../model/sql/v1/restfulService');

const env = process.env.NODE_ENV || 'local';
const config = require('../../config.json')[env];


function addSubtypeToDataItemMember(data) {
    const subTypeJson = data[0];
    subTypeJson[data[0].subType.toLowerCase()] = data[1];
    delete subTypeJson.subType;
    delete subTypeJson.dataCatalogId;
    return subTypeJson;
}

exports.getDataItem = (req, res) => {
    function getData(client) {
        const results = [];
        return utils.executeQuery(client, dataItemServiceSQL.dataItemSelectQuery(req.params.id), [
        ], 'No-Release').catch((e) => Promise.reject(new Error('Data Item Select Error')))
            .then(result => {
                results.push(_.first(result));
                if (results[0].subType === 'FILE') {
                    return utils.executeQuery(client, fileServiceSQL.dataItemFileSelectQuery(results[0].dataCatalogId), []).catch((e) => {
                        Promise.reject(new Error('Data Item File Select Error'));
                    });
                } else if (results[0].subType === 'DATABASE_OBJECT') {
                    return utils.executeQuery(client, databaseServiceSQL.databaseItemSelectQuery(results[0].dataCatalogId), []).catch((e) => {
                        Promise.reject(new Error('Data Item Database Select Error'));
                    });
                } else if (results[0].subType === 'URI') {
                    return utils.executeQuery(client, restfulServiceSQL.restfulItemSelectQuery(results[0].dataCatalogId), []).catch((e) => {
                        Promise.reject(new Error('Data Item Restful Select Error'));
                    });
                }
            })
            .then(result => {
                results.push(result);
                return {
                    id: `/dataItems/${req.params.id}`,
                    totalItems: results[0].length,
                    member: addSubtypeToDataItemMember(results),
                };
            });
    }
    return dbPool.connect(req.headers.nibr521).then(getData);
};

exports.deleteDataItem = (req, res) => {
    function deleteData(client) {
        const results = [];
        return utils.executeQuery(client, dataItemServiceSQL.dataItemDeleteQuery(req.params.id), [
        ], 'No-Release').catch((e) => Promise.reject(new Error('Data Item Delete Error')))
            .then(result => {
                results.push(_.first(result));
                if (results[0].subType === 'FILE') {
                    return utils.executeQuery(client, fileServiceSQL.dataItemFileDeleteQuery(results[0].dataCatalogId), []).catch((e) => {
                        Promise.reject(new Error('Data Item File Delete Error'));
                    });
                    // .then(data => {
                    //     results.push(_.first(data));
                    //     const params = { Bucket: config['data-bucket'], Key: results[1].storage_url };
                    //     return awsServices.s3config.deleteObject(params).promise().catch(() => Promise.reject(new Error('AWS Delete Error')));
                    // });
                    // TODO Delete file from S3 Bucket
                } else if (results[0].subType === 'DATABASE_OBJECT') {
                    return utils.executeQuery(client, databaseServiceSQL.databaseItemDeleteQuery(results[0].dataCatalogId), []).catch((e) => {
                        Promise.reject(new Error('Data Item Database Delete Error'));
                    });
                } else if (results[0].subType === 'URI') {
                    return utils.executeQuery(client, restfulServiceSQL.restfulItemDeleteQuery(results[0].dataCatalogId), []).catch((e) => {
                        Promise.reject(new Error('Data Item Restful Delete Error'));
                    });
                }
            })
            .then(result => {
                console.log(result);
                results.push(result);
                return {
                    id: `/dataItems/${req.params.id}`,
                    msg: 'Object Deleted',
                };
            });
    }
    return dbPool.connect(req.headers.nibr521).then(deleteData);
};

exports.putDataItem = (req, res) => {
    function updateCatalog(client) {
        return utils.executeQuery(client, dataItemServiceSQL.dataItemPutsQuery, [req.params.id,
            req.body.name, req.body.responsibleOrganizationCode, req.body.igmClassificationCode, req.body.sourceCode,
            req.body.dataFormatCode, req.body.privacyCategoryCode, req.body.domainCode])
            .catch((e) => Promise.reject(new Error('Data Update Error')))
            .then(data => ({ id: `/dataItems/${req.params.id}`,
                msg: 'Object Updated',
                data: data[0] }));
    }
    return dbPool.connect(req.headers.nibr521).then(updateCatalog);
};


exports.bulkGetItems = (req, res) => {
    function bulkGetCatalog(client) {
        return utils.executeQuery(client, dataItemServiceSQL.dataItemBulkGet, [req.body.ids])
            .catch((e) => Promise.reject(new Error('Data Bulk Retrieval Error')))
            .then(data => ({ id: '/dataItems',
                members: data }));
    }
    return dbPool.connect(req.headers.nibr521).then(bulkGetCatalog);
};


exports.bulkUpdateItems = (req, res) => {
    function bulkUpdateCatalog(client) {
        return utils.executeQuery(client, dataItemServiceSQL.dataItemBulkPost, [req.body.ids,
            req.body.responsibleOrganizationCode, req.body.igmClassificationCode, req.body.sourceCode,
            req.body.dataFormatCode, req.body.privacyCategoryCode, req.body.domainCode])
            .catch((e) => Promise.reject(new Error('Data Bulk Update Error')))
            .then(data => ({ id: '/dataItems',
                members: data }));
    }
    return dbPool.connect(req.headers.nibr521).then(bulkUpdateCatalog);
};
