
const _ = require('lodash');

const dbPool = require('../../db');
const utils = require('../common/utils');
const streamUtils = require('../common/streamUtils');
const kafka = require('../common/kafkaUtils');
//const logger = require('../middleware/logger'); if you want to use logger uncomment - these will show up in your cloudwatch log group if you have set this up
const commonSQL = require('../../model/sql/v1/common');
const databaseServiceSQL = require('../../model/sql/v1/databaseService');


const env = process.env.NODE_ENV || 'local';
const config = require('../../config.json')[env];

exports.dataItemCreate = (req, res) => {
    function insertDatabaseItem(client) {
        const results = [];
        return utils.executeQuery(client, commonSQL.insertDataItemQuerywoUUID, [
            req.body.name, req.headers.nibr521,
            req.body.responsibleOrganizationCode, req.body.sourceCode,
            req.body.dataFormatCode, 'DATABASE_OBJECT', req.body.igmClassificationCode,
            req.body.privacyCategoryCode, req.body.domainCode,
            req.headers.nibr521, req.headers.nibr521], 'No-Release')
            .catch((e) => Promise.reject(new Error('Data Item Insert Error')))
            .then(data => {
                results.push(_.first(data));
                return utils.executeQuery(client, databaseServiceSQL.insertDatabaseItemQuery, [
                    results[0].data_item_id, req.body.databaseType, req.body.schema, req.body.connectionString,
                    req.body.objectName, req.headers.nibr521, req.headers.nibr521])
                    .catch((e) => Promise.reject(new Error('Database Item Insert Error')));
            })
            .then(data => {
                results.push(data);
                //push new data item to kafka topic
                kafka.write2KafkaTopic('dcs', 'insert', results[0].data_item_key);
            })
            .then(data => {
                results.push(data);
                return { id: '/dataItems/database',
                    data_item_key: results[0].data_item_key,
                    msg: 'Database Object Created' };
            });
    }
    return dbPool.connect(req.headers.nibr521).then(insertDatabaseItem);
};

exports.dataItemUpdate = (req, res) => {
    function updateDatabaseItem(client) {
        const results = [];
        return utils.executeQuery(client, commonSQL.selectDataItemQuery,
            [req.params.id], 'No-Release')
            .catch((e) => Promise.reject(new Error('Data Item Select Error')))
            .then(data => {
                results.push(_.first(data));
                return utils.executeQuery(client, databaseServiceSQL.insertDatabaseItemQuery, [
                    results[0].data_item_id, req.body.databaseType, req.body.schema, req.body.connectionString,
                    req.body.objectName, req.headers.nibr521, req.headers.nibr521])
                    .catch((e) => Promise.reject(new Error('Data Item Database Object Update Error')));
            })
            .then(data => {
                results.push(data);
                return { id: `/dataItems/${req.params.id}/database`,
                    data_item_key: req.params.id,
                    msg: 'Object Updated' };
            });
    }
    return dbPool.connect(req.headers.nibr521).then(updateDatabaseItem);
};

exports.getDatabaseDetails = (req, res) => {
    function databaseSelectVersion(client) {
        return utils.executeQuery(client, databaseServiceSQL.dataItemDatabaseSelectQueryVersion, [req.params.id, req.query.version])
            .catch((e) => Promise.reject(new Error('DataBase Item Select Error')));
    }
    return dbPool.connect(req.headers.nibr521).then(databaseSelectVersion);
};

exports.getDatabaseVersions = (req, res) => {
    function databaseGetAllVersions(client) {
        return utils.executeQuery(client, databaseServiceSQL.dataItemDatabaseGetAllVersions, [req.params.id])
            .catch((e) => Promise.reject(new Error('DataBase Item Select Error')))
            .then(data => {
                const result = {
                    id: `/dataItems/${req.params.id}/database/versions`,
                    versions: data,
                };
                return result;
            });
    }
    return dbPool.connect(req.headers.nibr521).then(databaseGetAllVersions);
};
