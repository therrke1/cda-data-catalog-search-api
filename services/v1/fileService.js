
const _ = require('lodash');
const multer = require('multer');
const multerS3 = require('multer-s3');

const dbPool = require('../../db');
const utils = require('../common/utils');
const streamUtils = require('../common/streamUtils');
const kafka = require('../common/kafkaUtils');
const awsServices = require('../common/aws-services');
//const logger = require('../middleware/logger'); if you want to use logger uncomment - these will show up in your cloudwatch log group if you have set this up
const commonSQL = require('../../model/sql/v1/common');
const fileServiceSQL = require('../../model/sql/v1/fileService');

const env = process.env.NODE_ENV || 'local';
const config = require('../../config.json')[env];

exports.uploadCreate = () => multer({
    storage: multerS3({
        s3: awsServices.s3config,
        bucket: config['data-bucket'],
        contentType: multerS3.AUTO_CONTENT_TYPE,
        serverSideEncryption: 'AES256',
        metadata(req, file, callback) {
            callback(null, {});
        },
        key(req, file, callback) {
            req.params.fileuuid = utils.UUIDv4();
            const keyPathArray = [req.params.fileuuid, '/', file.originalname];
            callback(null, ''.concat(...keyPathArray));
        },
    }),
    limits: { fileSize: 5 * 1024 * 1024 * 1024 * 1024 },
}).single('file');


exports.DataItemCreate = (req, res) => {
    function insertFileDataItem(client) {
        const results = [];
        return utils.executeQuery(client, commonSQL.insertDataItemQuery, [
            req.params.fileuuid, req.body.name, req.headers.nibr521,
            req.body.responsibleOrganizationCode, req.body.sourceCode,
            req.body.dataFormatCode, 'FILE', req.body.igmClassificationCode,
            req.body.privacyCategoryCode, req.body.domainCode,
            req.headers.nibr521, req.headers.nibr521], 'No-Release')
            .catch((e) => Promise.reject(new Error('Data Item Insert Error')))
            .then(data => {
                results.push(_.first(data));
                const params = { Bucket: config['data-bucket'], Key: req.file.key };
                return awsServices.s3config.headObject(params).promise().catch(() => Promise.reject(new Error('AWS Error')));
            })
            .then(data => {
                results.push(data);
                return utils.executeQuery(client, fileServiceSQL.insertFileDataItemQuery, [
                    results[0].data_item_id, req.file.key, req.body.schema, req.file.etag.substring(1, req.file.etag.length - 1), results[1].VersionId,
                    req.headers.nibr521, req.headers.nibr521])
                    .catch((e) => Promise.reject(new Error('Data Item File Insert Error')));
            })
            .then(data => {
                results.push(data);
                //push new data item to kafka topic
                kafka.write2KafkaTopic('dcs', 'insert', results[0].data_item_key);
            })
            .then(data => {
                results.push(data);
                return { id: '/dataItems/file',
                    data_item_key: results[0].data_item_key,
                    msg: 'File Object Created' };
            });
    }
    return dbPool.connect(req.headers.nibr521).then(insertFileDataItem);
};

exports.uploadUpdate = (id) => multer({
    storage: multerS3({
        s3: awsServices.s3config,
        bucket: config['data-bucket'],
        contentType: multerS3.AUTO_CONTENT_TYPE,
        serverSideEncryption: 'AES256',
        metadata(req, file, callback) {
            callback(null, {});
        },
        key(req, file, callback) {
            const keyPathArray = [id, '/', file.originalname];
            callback(null, ''.concat(...keyPathArray));
        },
    }),
    limits: { fileSize: 5 * 1024 * 1024 * 1024 * 1024 },
}).single('file');


exports.DataItemUpdate = (req, res) => {
    function updateFileDataItem(client) {
        const results = [];
        return utils.executeQuery(client, commonSQL.selectDataItemQuery,
            [req.params.id], 'No-Release')
            .catch((e) => Promise.reject(new Error('Data Item Select Error')))
            .then(data => {
                results.push(_.first(data));
                const params = { Bucket: config['data-bucket'], Key: req.file.key };
                return awsServices.s3config.headObject(params).promise().catch((e) => Promise.reject(new Error('AWS Error')));
            })
            .then(data => {
                results.push(data);
                return utils.executeQuery(client, fileServiceSQL.insertFileDataItemQuery, [
                    results[0].data_item_id, req.file.key, req.body.schema, req.file.etag.substring(1, req.file.etag.length - 1), data.VersionId,
                    req.headers.nibr521, req.headers.nibr521]).catch((e) => Promise.reject(new Error('Data Item File Update Error')));
            })
            .then(data => {
                results.push(data);
                return { id: `/dataItems/${req.params.id}/file`,
                    data_item_key: req.params.id,
                    msg: 'Object Updated' };
            });
    }
    return dbPool.connect(req.headers.nibr521).then(updateFileDataItem);
};

exports.GetFileDetails = (req, res) => {
    function fileSelectVersion(client) {
        // console.log(req.query.version)
        return utils.executeQuery(client, fileServiceSQL.dataItemFileSelectQueryVersion, [req.params.id, req.query.version])
            .catch((e) => Promise.reject(new Error('Data Item FileSelect Error')));
    }
    return dbPool.connect(req.headers.nibr521).then(fileSelectVersion);
};

exports.downloadFile = (req, res, data) => {
    const storageUrlArray = data[0].storageUrl.split('/');
    res.attachment(storageUrlArray[storageUrlArray.length - 1]);
    const params = {
        Bucket: config['data-bucket'],
        Key: data[0].storageUrl,
    };
    const fileStream = awsServices.s3config.getObject(params).createReadStream();
    return fileStream;
};


exports.getFileVersions = (req, res) => {
    function filegetAllVersions(client) {
        return utils.executeQuery(client, fileServiceSQL.dataItemFileGetAllVersions, [req.params.id])
            .catch((e) => Promise.reject(new Error('Data Item FileSelect Error')))
            .then(data => {
                const result = {
                    id: `/dataItems/${req.params.id}/file/versions`,
                    versions: data,
                };
                return result;
            });
    }
    return dbPool.connect(req.headers.nibr521).then(filegetAllVersions);
};
