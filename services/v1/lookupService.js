//if you want to use logger uncomment - these will show up in your cloudwatch
// log group if you have set this up
//const logger = require('../middleware/logger');
const dbPool = require('../../db');
const utils = require('../../services/common/utils');
const _ = require('lodash');
const lookupSQL = require('../../model/sql/v1/lookupService');

/**
 * Each object value should match corresponding lookup table name.
 */
const LOOKUP_TYPES = {
    dataDomain: 'data_domain',
    dataFormat: 'data_format',
    igmClassification: 'igm_classification',
    privacyCategory: 'privacy_category',
    source: 'source',
};

exports.LOOKUP_TYPES = LOOKUP_TYPES;

/**
 * @param {string} lookupType - Should be one of LOOKUP_TYPES
 * @param {string} url - route
 */
exports.getLookups = (lookupType, url) => {
    const getData = (client) => {
        if (!_.includes(LOOKUP_TYPES, lookupType)) {
            throw utils.createErrorObject(400, 'Invalid lookup type');
        }

        return dbPool.executeQuery(client,
            lookupSQL.getSelectLookupQuery(lookupType));
    };

    const transformLookup = (lookups) => ({
        id: url,
        totalItems: lookups.length,
        member: lookups.map(val => ({
            code: val.code,
            name: val.name,
            description: val.description,
            contact: val.contact,
            id: val[`${lookupType}_id`],
        })),
    });

    return dbPool.connect().then(getData).then(transformLookup);
};

/**
 * Lookup id specified - UPDATE operation,
 * otherwise - INSERT.
 *
 * @param {object[]} lookups
 * @param {string} lookups[0].code
 * @param {string} lookups[0].name
 * @param {string|undefined} lookups[0].id
 * @param {string|undefined} lookups[0].description - Source lookup only
 * @param {string|undefined} lookups[0].contact - Source lookup only
 * @param {string|undefined} lookups[0].url - Source lookup only
 *
 * @param {string} lookupType - Should be one of LOOKUP_TYPES
 * @param {string} nibr521
 */
exports.saveOrUpdateLookups = (lookups, lookupType, nibr521) => {
    function saveData(client) {
        if (_.isNil(lookups) || _.isEmpty(lookups)) {
            throw utils.createErrorObject(400,
                'You should provide at least one lookup object');
        }
        if (!nibr521) {
            throw utils.createErrorObject(400, 'nibr521 is undefined');
        }
        if (!_.includes(LOOKUP_TYPES, lookupType)) {
            throw utils.createErrorObject(400, 'Invalid lookup type');
        }

        const batchQueries = lookups.map(lookup => {
            if (lookup.id) {
                return lookupType === LOOKUP_TYPES.source
                    ? {
                        query: lookupSQL.getUpdateSourceLookupQuery(),
                        values: [lookup.code.toUpperCase(), lookup.name,
                            lookup.description, lookup.contact, lookup.url,
                            nibr521],
                    }
                    : {
                        query: lookupSQL.getUpdateLookupQuery(lookupType),
                        values: [lookup.id, lookup.code.toUpperCase(),
                            lookup.name, nibr521],
                    };
            }

            return lookupType === LOOKUP_TYPES.source
                ? {
                    query: lookupSQL.getInsertSourceLookupQuery(),
                    values: [lookup.code.toUpperCase(), lookup.name,
                        lookup.description, lookup.contact, lookup.url,
                        nibr521],
                }
                : {
                    query: lookupSQL.getInsertLookupQuery(lookupType),
                    values: [lookup.code.toUpperCase(), lookup.name, nibr521],
                };
        });

        return dbPool.commitTransaction(client, batchQueries);
    }

    return dbPool.connectTransaction(nibr521).then(saveData);
};

/**
 *
 * {string} @param id - lookup id
 */
exports.deleteLookup = (id, lookupType, nibr521) => {
    function deleteData(client) {
        if (!_.includes(LOOKUP_TYPES, lookupType)) {
            throw utils.createErrorObject(400, 'Invalid lookup type');
        }
        return dbPool.executeQuery(client,
            lookupSQL.getDeleteLookupQuery(lookupType), [id]);
    }

    return dbPool.connect(nibr521).then(deleteData);
};
