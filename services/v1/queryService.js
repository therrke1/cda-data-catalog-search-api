
const _ = require('lodash');

const dbPool = require('../../db');
const utils = require('../common/utils');
const streamUtils = require('../common/streamUtils');
//const logger = require('../middleware/logger'); if you want to use logger uncomment - these will show up in your cloudwatch log group if you have set this up
const commonSQL = require('../../model/sql/v1/common');
const querySQL = require('../../model/sql/v1/queryService');

exports.queryDataCatalog = (req, res) => {
    function queryingCatalog(client) {
        const results = [];
        return utils.executeQuery(client, querySQL.queryCatalog, [
            req.query.responsibleOrganizationCodes, req.query.domainCodes,
            req.query.igmClassificationCodes, req.query.privacyCategoryCodes,
            req.query.entityTypes, req.query.entityInstances])
            .catch((e) => {
                console.log(e);
                return Promise.reject(new Error('Data Query Error'));
            })
            .then(data => ({ id: '/query',
                totalItems: data.length,
                member: data }));
    }
    return dbPool.connect(req.headers.nibr521).then(queryingCatalog);
};

