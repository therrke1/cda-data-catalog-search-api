
const _ = require('lodash');

const dbPool = require('../../db');
const utils = require('../common/utils');
const streamUtils = require('../common/streamUtils');
const kafka = require('../common/kafkaUtils');
//const logger = require('../middleware/logger'); if you want to use logger uncomment - these will show up in your cloudwatch log group if you have set this up
const commonSQL = require('../../model/sql/v1/common');
const restfulServiceSQL = require('../../model/sql/v1/restfulService');

const env = process.env.NODE_ENV || 'local';
const config = require('../../config.json')[env];

exports.dataItemCreate = (req, res) => {
    function insertRestfulItem(client) {
        const results = [];
        return utils.executeQuery(client, commonSQL.insertDataItemQuerywoUUID, [
            req.body.name, req.headers.nibr521,
            req.body.responsibleOrganizationCode, req.body.sourceCode,
            req.body.dataFormatCode, 'URI', req.body.igmClassificationCode,
            req.body.privacyCategoryCode, req.body.domainCode,
            req.headers.nibr521, req.headers.nibr521], 'No-Release')
            .catch((e) => Promise.reject(new Error('Data Item Insert Error')))
            .then(data => {
                results.push(_.first(data));
                return utils.executeQuery(client, restfulServiceSQL.insertRestfulItemQuery, [
                    results[0].data_item_id, req.body.urlEndpoint, req.body.schema,
                    req.headers.nibr521, req.headers.nibr521])
                    .catch((e) => Promise.reject(new Error('Restful Item Insert Error')));
            })
            .then(data => {
                results.push(data);
                //push new data item to kafka topic
                kafka.write2KafkaTopic('dcs', 'insert', results[0].data_item_key);
            })
            .then(data => {
                results.push(data);
                return { id: '/dataItems/restful',
                    data_item_key: results[0].data_item_key,
                    msg: 'Restful Object Created' };
            });
    }
    return dbPool.connect(req.headers.nibr521).then(insertRestfulItem);
};

exports.dataItemUpdate = (req, res) => {
    function updateRestfulItem(client) {
        const results = [];
        return utils.executeQuery(client, commonSQL.selectDataItemQuery,
            [req.params.id], 'No-Release')
            .catch((e) => Promise.reject(new Error('Data Item Select Error')))
            .then(data => {
                results.push(_.first(data));
                return utils.executeQuery(client, restfulServiceSQL.insertRestfulItemQuery, [
                    results[0].data_item_id, req.body.urlEndpoint,
                    req.headers.nibr521, req.headers.nibr521])
                    .catch((e) => Promise.reject(new Error('Data Item Restful Update Error')));
            })
            .then(data => {
                results.push(data);
                return { id: `/dataItems/${req.params.id}/restful`,
                    data_item_key: req.params.id,
                    msg: 'Object Updated' };
            });
    }
    return dbPool.connect(req.headers.nibr521).then(updateRestfulItem);
};

exports.getRestfulDetails = (req, res) => {
    function restfulSelectVersion(client) {
        return utils.executeQuery(client, restfulServiceSQL.dataItemRestfulSelectQueryVersion, [req.params.id, req.query.version])
            .catch((e) => Promise.reject(new Error('Restful Item Select Error')));
    }
    return dbPool.connect(req.headers.nibr521).then(restfulSelectVersion);
};

exports.getRestfulVersions = (req, res) => {
    function restfulGetAllVersions(client) {
        return utils.executeQuery(client, restfulServiceSQL.dataItemRestfulGetAllVersions, [req.params.id])
            .catch((e) => Promise.reject(new Error('Restful Item Select Error')))
            .then(data => {
                const result = {
                    id: `/dataItems/${req.params.id}/restful/versions`,
                    versions: data,
                };
                return result;
            });
    }
    return dbPool.connect(req.headers.nibr521).then(restfulGetAllVersions);
};
