const v1lookupService = require('../services/v1/lookupService');
const dbPool = require('../db');
const utils = require('../services/common/utils');

const chai = require('chai');
chai.use(require('chai-things'));
chai.use(require('chai-as-promised'));

chai.should();

const LOOKUP_TYPES = v1lookupService.LOOKUP_TYPES;

describe('Lookup services Test Cases', function () {
    this.timeout(10000);

    describe('#getLookups()', () => {
        it('should return all Domains',
            () => v1lookupService.getLookups(LOOKUP_TYPES.dataDomain)
                .should.eventually.be.an('object')
                .and.include.property('totalItems'));

        it('should return all Formats',
            () => v1lookupService.getLookups(LOOKUP_TYPES.dataFormat)
                .should.eventually.be.an('object')
                .and.include.property('totalItems'));

        it('should return all Igm Classifications',
            () => v1lookupService.getLookups(LOOKUP_TYPES.igmClassification)
                .should.eventually.be.an('object')
                .and.include.property('totalItems'));

        it('should return all Privacy Categories',
            () => v1lookupService.getLookups(LOOKUP_TYPES.privacyCategory)
                .should.eventually.be.an('object')
                .and.include.property('totalItems'));

        it('should return all Sources',
            () => v1lookupService.getLookups(LOOKUP_TYPES.source)
                .should.eventually.be.an('object')
                .and.include.property('totalItems'));
    });

    after(() => {
    //Clean up if needed
    /*return dbPool.connect()
      .then(client => {
        const deleteExampleQuery = `
          DELETE FROM example
          WHERE "id" = $1
          RETURNING *;`;

        return utils.executeQuery(client, deleteExampleQuery, [1]);
      });*/
    });
});
