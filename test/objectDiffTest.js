const objectDiff = require('../common/objectDiff');

const chai = require('chai');

chai.use(require('chai-things'));
chai.use(require('chai-subset'));

chai.should();

describe('Object diff service', () => {
    describe('Test equal objects', () => {
        it('Test complex objects', () => objectDiff.diff({ one: 5, two: 'test' }, { one: 5, two: 'test' })
      .should.be.an('object')
      .that.containSubset({ changed: 'equal' }));
        it('Test complex objects with high nesting level', () => objectDiff.diff({ one: 5, two: 'test', three: { four: 'string', five: true, six: { seven: 5.6 } } }, { one: 5, two: 'test', three: { four: 'string', five: true, six: { seven: 5.6 } } })
      .should.be.an('object')
      .that.containSubset({ changed: 'equal' }));
    });
    describe('Test non equal objects', () => {
        it('Test complex objects', () => objectDiff.diff({ one: 6, two: 'test' }, { one: 5, two: 'test' })
      .should.be.an('object')
      .that.containSubset({ changed: 'object change' }));
        it('Test complex objects with high nesting level', () => objectDiff.diff({ one: 5, two: 'test', three: { four: 'ing', five: true, six: { seven: 5.6 } } }, { one: 5, two: 'test', three: { four: 'string', six: { seven: 5.6 } } })
      .should.be.an('object')
      .that.containSubset({ changed: 'object change' }));
    });
});
