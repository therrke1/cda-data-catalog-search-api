const utils = require('../services/common/utils');
const dbPool = require('../db');

const chai = require('chai');
chai.use(require('chai-as-promised'));

const expect = chai.expect;

chai.should();

describe('common/utils', function () {
    this.timeout(10000);

    describe('#createErrorObject()', () => {
        it('should return error object', () => {
            expect(utils.createErrorObject(500, 'test'))
        .to.be.an('error')
        .that.has.property('status', 500);
        });
    });

    describe('#flattenProperties()', () => {
        it('should merge properties to root object', () => {
            expect(utils.flattenProperties([{ a: 1, properties: { x: 2, y: { z: 3 } } }]))
        .to.deep.equal([{ a: 1, x: 2, y: { z: 3 } }]);
        });
    });

    describe('#executeQuery()', () => {
        it('should connect to db and execute simple query', () => dbPool.connect().then(client => utils.executeQuery(client, 'SELECT 1 AS "a" LIMIT 1;'))
        .should.eventually.deep.equal([{ a: 1 }]));
    });

    describe('#getFacets()', () => {
        it('should get ordered list of available values by array of attributes', () => {
            expect(utils.getFacets([{ a: '1', b: 'b' }, { a: 'a', b: '2', c: 'c' }, { a: '1', b: 'b' }], ['a', 'b']))
        .to.deep.equal({ a: ['1', 'a'], b: ['2', 'b'] });
        });
    });

    describe('#stripStartingIn()', () => {
        it('should strip starting "in " from string', () => {
            expect(utils.stripStartingIn('in MOLECULAR_CREATION,COMPOUND,COMPOUND_INDICATION'))
        .to.equal('MOLECULAR_CREATION,COMPOUND,COMPOUND_INDICATION');
        });
        it('should not change anything if not starts with "in "', () => {
            expect(utils.stripStartingIn('INDICATION,COMPOUND,COMPOUND_INDICATION'))
        .to.equal('INDICATION,COMPOUND,COMPOUND_INDICATION');
        });
    });

    describe('#generateParameters()', () => {
        it('should return ""', () => {
            expect(utils.generateParameters([]))
        .to.equal('');
        });
        it('should return "$1, $2"', () => {
            expect(utils.generateParameters(['bla', 'bla']))
        .to.equal('$1, $2');
        });
    });
});
