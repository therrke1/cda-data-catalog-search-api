# Installation

### npm

By default, npm pulls packages from the official
registry: registry.npmjs.org. However, NIBR offers an internal
registry with all public packages available, plus additional
NIBR specific packages written by employees.

Run the following command to tell NPM to use our Artifactory registry:
```
 npm config set registry http://repo.nibr.novartis.net/artifactory/api/npm/npm
 ```

Configure npm to use the correct NIBR proxies

```
npm config set proxy http://nibr-proxy.global.nibr.novartis.net:2011/
npm config set https-proxy http://nibr-proxy.global.nibr.novartis.net:2011/
```


### app

Run the following command to install project

```
npm install
```


#### dev mode

If you want to run project locally, you have to specify your local api url in
```
./src/js/ducks/helpers/apiCalls.js
```

Run project
```
npm start
```
By default it runs on 3000 port.

#### production mode
Run the following command to deploy ui artifacts to project public:

```
npm run build
```
