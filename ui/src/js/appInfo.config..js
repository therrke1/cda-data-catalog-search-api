import React from 'react';

const version = require('../../../package.json').version;

const getEnvironment = () => {
    const host = document.location.hostname;

    // todo define environment matching
    return host.match(/local/) ? 'local' : 'dev';
};

export const appInfo = {
    getHelp: {
        subHeading: 'How can the CDADCS team help you?',
        subHeadingText: 'Please fill out the form to contact the CDADCS team about issues or feature requests.',
        otherResources: (
            <ul style={{ listStyle: 'none', paddingLeft: '0px' }}>
                <li>
                    <a
                        target="_blank"
                        rel="noopener noreferrer"
                        href="http://go/wag-docs"
                    >WAG Documentation</a>
                </li>
            </ul>
        ),
        channelOptions: {
            issue: {
                type: 'JIRA',
                value: {
                    issueTypeName: 'Bug',
                    defaultAssignee: 'westeja2',
                    projectCode: 'DSG',
                },
            },
            request: {
                type: 'Email',
                // this will fail, so change it or remove
                value: 'ENTER_VALID_EMAIL@novartis.com',
            },
            feedback: {
                type: 'JIRA',
                value: {
                    issueTypeName: 'Feature',
                    defaultAssignee: 'westeja2',
                    projectCode: 'WAG',
                },
            },
        },
    },
    appName: 'CDA Data Catalog Service',
    appDescription: 'CDA Data Catalog Service',
    appTitle: 'CDA Data Catalog Service',
    appDeployState: getEnvironment(),
    version,
    apiUrl: getEnvironment() === 'local'
        ? 'http://10.188.165.81:8080' // provide your own local url
        : '',
};

export default appInfo;
