import React from 'react';
import appInfo from '../appInfo.config.';

const Footer = () => {
    const styles = {
        root: {
            position: 'absolute',
            bottom: '0px',
            left: '0px',
            width: '100vw',
            height: '35px',
            borderTop: '1px solid #EDEDED',
            display: 'flex',
            padding: '1em',
            alignItems: 'center',
            fontSize: '10px',
            color: '#C6C6C6',
            backgroundColor: 'white',
            justifyContent: 'space-between',
        },
    };


    return (
        <div style={styles.root}>
            <div>
                <span>CDA Data Catalog Service. Version: </span>
                <code>{appInfo.version}</code>
            </div>
            <div style={{ marginRight: '20px' }}>
                <a href={`${appInfo.apiUrl}/docs`}>
                    API Documentation
                </a>
            </div>
        </div>
    );
};

export default Footer;
