import React from 'react';

const Forbidden = () => (
    <div className="jumbotron">
        <h1>403 Forbidden</h1>
        <h2>You do not have access to this page</h2>
    </div>
);

export default Forbidden;
