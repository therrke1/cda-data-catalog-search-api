import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';

const SingleSelect = (props) => (
    <Select
        {...props}
        value={props.value}
        searchable={props.searchable}
        clearable={props.clearable}
        onChange={props.actionCallback}
        className="cda-select"
    />
);

SingleSelect.propTypes = {
    options: PropTypes.arrayOf(
        PropTypes.shape({
            value: PropTypes.string.isRequired,
            label: PropTypes.string.isRequired,
        })
    ).isRequired,
    actionCallback: PropTypes.func.isRequired,
    clearable: PropTypes.bool,
    value: PropTypes.shape({
        value: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
    }).isRequired,
    searchable: PropTypes.bool,
};

SingleSelect.defaultProps = {
    clearable: false,
    searchable: false,
};

export default SingleSelect;
