import appInfo from '../../appInfo.config.';


const URL_PREFIX = `${appInfo.apiUrl}/v1`;

const validateResponse = (data) => {
    if (!data.ok) {
        const message = `Status: ${data.status}. ${data.statusText}`;
        throw new Error(message);
    }
};

const sendRequest = (url, opts = {}) => fetch(url,
    {
        ...opts,
        credentials: opts.credentials || 'same-origin',
        method: opts.method || 'GET',
    });


const sendGET = (url, opts) =>
    sendRequest(`${URL_PREFIX}${url}`, opts)
        .then(data => {
            validateResponse(data);
            return data.json();
        });

const sendDELETE = (url, opts) =>
    sendRequest(`${URL_PREFIX}${url}`, { ...opts, method: 'DELETE' })
        .then(data => {
            validateResponse(data);
        });

const sendPOST = (url, opts) =>
    sendRequest(`${URL_PREFIX}${url}`, { ...opts, method: 'POST' })
        .then(data => {
            validateResponse(data);
        });

/**
 * lookups
 */
export const getLookup = (value) => sendGET(`/${value}`);
export const insertLookup = (value, lookups) => sendPOST(`/${value}`, {
    body: JSON.stringify({ lookups }),
    headers: {
        'Content-Type': 'application/json',
    },
});

export const updateLookup = (value, body) => sendGET(`/${value}`, {
    method: 'PUT',
    body: JSON.stringify(body),
});
export const deleteLookup = (value, id) => sendDELETE(`/${value}/${id}`);


/**
 * authorization
 */
export const getUserPermissions = (nibr521) =>
    sendGET(`/authorization/${nibr521}`);
