import { call, put, takeEvery } from 'redux-saga/effects';

import SagasManager from '../helpers/SagasManager';
import * as API from '../helpers/apiCalls';

const GET_PERSON = 'CDA/auth/GET_PERSON';
const GET_PERMISSIONS = 'CDA/auth/GET_PERMISSIONS';

export const getPerson = (person) => ({
    type: GET_PERSON,
    payload: person,
});

const initState = {
    person: '',
    error: null,
    permissions: [],
    fetched: false,
};

const auth = (state = initState, action) => {
    switch (action.type) {
        case GET_PERSON:
            return { ...state, person: action.payload };
        case `${GET_PERMISSIONS}_FULFILLED`:
            return { ...state, permissions: action.payload, fetched: true };
        case `${GET_PERMISSIONS}_REJECTED`:
            return { ...state, error: action.payload, fetched: false };
        default:
            return state;
    }
};


export function* authorizePerson(action) {
    try {
        const nibr521 = action.payload.NIBR521;
        const data = yield call(API.getUserPermissions, nibr521);
        yield put({
            type: `${GET_PERMISSIONS}_FULFILLED`,
            payload: data,
        });
    } catch (error) {
        yield put({ type: `${GET_PERMISSIONS}_REJECTED`, payload: error });
    }
}

SagasManager.addSagaToRoot(function* watcher() {
    yield takeEvery(GET_PERSON, authorizePerson);
});


export default auth;
