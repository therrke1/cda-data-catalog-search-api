import { call, put, takeEvery, select } from 'redux-saga/effects';

import SagasManager from '../helpers/SagasManager';
import * as API from '../helpers/apiCalls';

const GET_LOOKUP = 'CDA/lookup/GET_LOOKUP';
const DELETE_LOOKUP = 'CDA/lookup/DELETE_LOOKUP';
const ADD_LOOKUP = 'CDA/lookup/ADD_LOOKUP';

const UPDATE_ROW = 'CDA/lookup/UPDATE_ROW';

export const getLookup = (lookup) => ({
    type: GET_LOOKUP,
    payload: lookup,
});

export const deleteLookup = (id) => ({
    type: DELETE_LOOKUP,
    payload: id,
});

export const addLookup = (lookup) => ({
    type: ADD_LOOKUP,
    payload: lookup,
});

export const catchError = (error, actionName) => ({
    type: `${actionName}_REJECTED`,
    payload: error,
});

export const updateLookupRow = (id, columnName, columnValue) => ({
    type: UPDATE_ROW,
    payload: { id, columnValue, columnName },
});

const lookups = [{
    value: 'domains', // value should match api endings. Example: value = domains for '/v1/domains' route
    label: 'Domain',
}, {
    value: 'formats',
    label: 'Format',
}, {
    value: 'igmClassifications',
    label: 'IGM Classification',
}, {
    value: 'privacyCategories',
    label: 'Privacy Category',
}, {
    value: 'sources',
    label: 'Sources',
}];

const initState = {
    error: null,
    selectedLookup: lookups[0],
    lookups,
    data: null,
    lookupValues: {},
    updatedRows: {},
};

const lookup = (state = initState, action) => {
    switch (action.type) {
        case `${GET_LOOKUP}_FULFILLED`: {
            const lookupValues = {};

            action.payload.data.member.forEach(item => {
                const entries = Object.entries(item);
                entries.forEach(entry => {
                    if (!lookupValues[entry[0]]) {
                        lookupValues[entry[0]] = [entry[1]];
                    } else {
                        lookupValues[entry[0]].push(entry[1]);
                    }
                });
            });

            return {
                ...state,
                lookupValues,
                selectedLookup: action.payload.selectedLookup,
                data: action.payload.data,
                updatedRows: {} };
        }
        case `${GET_LOOKUP}_REJECTED`:
        case `${DELETE_LOOKUP}_REJECTED`:
        case `${ADD_LOOKUP}_REJECTED`:
            return { ...state, error: action.payload };

        case UPDATE_ROW: {
            const { id, columnName, columnValue } = action.payload;

            const newState = { ...state.updatedRows };
            if (!newState[id]) {
                newState[id] = {};
            }

            newState[id][columnName] = columnValue;

            return { ...state, updatedRows: newState };
        }
        default:
            return state;
    }
};

const selectLookup = (state) => state.lookup.selectedLookup;

export function* fetchLookup(action) {
    try {
        const data = yield call(API.getLookup, action.payload.value);
        yield put({
            type: `${GET_LOOKUP}_FULFILLED`,
            payload: { selectedLookup: action.payload, data },
        });
    } catch (error) {
        yield put(catchError(error, GET_LOOKUP));
    }
}

export function* removeLookup(action) {
    try {
        const selectedLookup = yield select(selectLookup);
        yield call(API.deleteLookup, selectedLookup.value, action.payload);
    } catch (error) {
        yield put(catchError(error, DELETE_LOOKUP));
    }
}

export function* insertLookup(action) {
    try {
        const selectedLookup = yield select(selectLookup);
        yield call(API.insertLookup, selectedLookup.value, [action.payload]);
        yield put(getLookup(selectedLookup));
    } catch (error) {
        yield put(catchError(error, ADD_LOOKUP));
    }
}

SagasManager.addSagaToRoot(function* watcher() {
    yield takeEvery(GET_LOOKUP, fetchLookup);
});

SagasManager.addSagaToRoot(function* watcher() {
    yield takeEvery(DELETE_LOOKUP, removeLookup);
});

SagasManager.addSagaToRoot(function* watcher() {
    yield takeEvery(ADD_LOOKUP, insertLookup);
});

export default lookup;
