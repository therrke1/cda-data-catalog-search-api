import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import lookup from './modules/lookup';
import auth from './modules/auth';


export default combineReducers({
    routing: routerReducer,
    lookup,
    auth,
});
