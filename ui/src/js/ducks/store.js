import { createStore, compose, applyMiddleware } from 'redux';
import { syncHistoryWithStore, routerMiddleware } from 'react-router-redux';
import { useRouterHistory } from 'react-router';
import createSagaMiddleware, { END } from 'redux-saga';
import logger from 'redux-logger';

import createHistory from 'history/lib/createHashHistory';
import reducer from './reducers';
import appInfo from '../appInfo.config.';

const history = useRouterHistory(createHistory)();

export default function createStoreAndHistory(initialState) {
    const sagaMiddleware = createSagaMiddleware();

    const middleware = appInfo.appDeployState === 'local'
        // logger should be the last middleware
        ? applyMiddleware(sagaMiddleware, routerMiddleware(history), logger)
        : applyMiddleware(sagaMiddleware, routerMiddleware(history));

    const store = createStore(
        reducer,
        initialState,
        compose(middleware),
    );
    const syncedHistory = syncHistoryWithStore(history, store);

    store.runSaga = sagaMiddleware.run;
    store.close = () => store.dispatch(END);

    return { store, history: syncedHistory };
}
