import 'babel-polyfill';
import { Router } from 'react-router';
import { Provider } from 'react-redux';
import React from 'react';
import ReactDOM from 'react-dom';

import createStoreAndHistory from './ducks/store';
import routes from './routes';
import SagasManager from './ducks/helpers/SagasManager';

require('isomorphic-fetch');

const { store, history } = createStoreAndHistory();
store.runSaga(SagasManager.getRootSaga());

ReactDOM.render(
    <Provider store={store}>
        <div>
            <Router history={history}>
                { routes }
            </Router>
        </div>
    </Provider>,
    document.getElementById('content')
);
