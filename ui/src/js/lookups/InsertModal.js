import React from 'react';
import PropTypes from 'prop-types';

import { capitalize } from '../helpers/utils';

export default class InsertModal extends React.Component {
    static propTypes = {
        columns: PropTypes.array.isRequired,
        data: PropTypes.array.isRequired,
        onAddRow: PropTypes.func.isRequired,
        onModalClose: PropTypes.func.isRequired,
        columnValues: PropTypes.object.isRequired,
    };

    constructor(props) {
        super(props);
        const state = {
            errorMessages: [],
        };

        props.columns.forEach(column => {
            if (column.field !== 'key') {
                state[column.field] = '';
            }
        });

        this.state = state;
    }

    onInputChange = (field) =>
        (e) => {
            const newState = {
                errorMessages: this.state.errorMessages,
            };

            const value = field === 'code'
                ? e.target.value.toUpperCase()
                : e.target.value;

            const errorMsg = `${capitalize(field)} should be unique!`;
            const isError = !this.isValuesUnique(field, value);

            newState[field] = value;

            if (isError) {
                if (!newState.errorMessages.includes(errorMsg)) {
                    newState.errorMessages.push(errorMsg);
                }
            } else {
                newState.errorMessages =
                    newState.errorMessages.filter(msg => msg !== errorMsg);
            }


            this.setState(newState);
        };

    handleSaveBtnClick = () => {
        this.props.onAddRow(this.state);
        this.props.onModalClose();
    };

    isValuesUnique = (field, value) =>
        !this.props.columnValues[field].includes(value);

    isSaveDisabled = () => {
        const state = this.state;
        if (state.errorMessages.length) {
            return true;
        }

        let disabled = false;
        const entries = Object.entries(state);

        entries.forEach(entry => {
            if (entry[0] === 'errorMessage') {
                return;
            }

            if (!entry[1]) {
                disabled = true;
            }
        });

        return disabled;
    };

    render() {
        const props = this.props;
        const state = this.state;

        const columns = props.columns.map((column) => {
            const { field, name } = column;
            if (field === 'key') {
                return null;
            }
            return (
                <div className="form-group" key={field}>
                    <label>{name}</label>
                    <input
                        value={state[field]}
                        className="form-control editor edit-text"
                        onChange={this.onInputChange(field)}
                    />
                </div>
            );
        });

        const errors = state.errorMessages.map(err => (
            <span className="help-block" style={{ color: 'red' }} key={err}>
                {err}
            </span>
        ));
        return (
            <div className="modal-content">
                <div className="modal-header">
                    <span>
                        <h4 className="modal-title">Add lookup</h4>
                    </span>
                </div>
                <div className="modal-body">
                    {columns}
                    {errors}
                </div>
                <div className="modal-footer">
                    <span>
                        <button
                            className="btn btn-default"
                            onClick={props.onModalClose}
                        >Cancel</button>
                        <button
                            className="btn btn-primary"
                            onClick={this.handleSaveBtnClick}
                            disabled={this.isSaveDisabled()}
                        >Save</button>
                    </span>
                </div>
            </div>
        );
    }
}
