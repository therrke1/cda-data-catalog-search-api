import React from 'react';
import PropTypes from 'prop-types';
import {
    BootstrapTable, TableHeaderColumn, SearchField,
} from 'react-bootstrap-table';

import { capitalize } from '../helpers/utils';
import InsertModal from './InsertModal';


export class LookupTable extends React.Component {
    static propTypes = {
        data: PropTypes.arrayOf(
            PropTypes.shape(
                PropTypes.string.isRequired,
                PropTypes.string.isRequired,
            )
        ).isRequired,
        onUpdateRow: PropTypes.func.isRequired,
        onAddRow: PropTypes.func.isRequired,
        onDeleteRow: PropTypes.func.isRequired,
        adminMode: PropTypes.bool,
        lookupValues: PropTypes.object.isRequired,
    };

    static defaultProps = {
        adminMode: false,
    };

    createCustomInsertButton = (onClick) => (
        <button className="btn btn-primary" onClick={onClick}>
            Add new <span className="glyphicon glyphicon-plus" />
        </button>
    );

    createCustomDeleteButton = (onClick) => (
        <button className="btn btn-danger" onClick={onClick}>
            Delete <span className="glyphicon glyphicon-trash" />
        </button>
    );

    createCustomSearchField = (props) => (
        <SearchField
            defaultValue={props.defaultSearch}
            placeholder="Filter"
        />
    );

    createCustomModal = (onModalClose, onSave, columns, validateState,
        ignoreEditable) => {
        const attrs = {
            onModalClose, onSave, columns, validateState, ignoreEditable,
        };
        return (
            <InsertModal
                {...attrs}
                data={this.props.data}
                onAddRow={this.props.onAddRow}
                columnValues={this.props.lookupValues}
            />
        );
    };

    validateCell = (value) => {
        if (!value) {
            return 'Field shouldn\'t be empty!';
        }
        return true;
    };


    formatCell = (key) => {
        const style = key === 'code'
            ? { textTransform: 'uppercase' }
            : {};

        return (cell) => <div style={style}>{cell}</div>;
    };

    render() {
        const props = this.props;
        let rowKey = -1;
        const data = props.data.map((item) => {
            rowKey += 1;
            return { ...item, key: rowKey };
        });

        const opts = {
            defaultSortName: null,
            defaultSortOrder: null,
            afterInsertRow: props.onAddRow,
            afterDeleteRow: props.onDeleteRow,
            clearSearch: true,
            insertBtn: this.createCustomInsertButton,
            deleteBtn: this.createCustomDeleteButton,
            searchField: this.createCustomSearchField,
            insertModal: this.createCustomModal,
        };

        const cellEditProp = props.adminMode
            ? {
                mode: 'dbclick',
                afterSaveCell: props.onUpdateRow,
            }
            : {};

        const selectRow = props.adminMode
            ? {
                mode: 'radio',
            }
            : {};

        const columns = [(
            <TableHeaderColumn
                dataField="key"
                searchable={false}
                width="30"
                dataAlign="center"
                key="key"
                autoValue={() => {
                    rowKey += 1;
                    return rowKey;
                }}
            >#</TableHeaderColumn>
        )];

        if (data.length) {
            const keys = Object.keys(data[0]);
            keys.forEach(key => {
                if (key === 'key' || key === 'id') return;

                columns.push(
                    <TableHeaderColumn
                        dataField={key}
                        dataSort
                        key={key}
                        editable={{ validator: this.validateCell }}
                        dataFormat={this.formatCell(key)}
                    >{capitalize(key)}</TableHeaderColumn>
                );
            });
        }

        return (
            <div className="row lookup-table">
                <div className="col-md-12 col-lg-12 col-sm-12">
                    <BootstrapTable
                        data={data}
                        keyField="key"
                        options={opts}
                        cellEdit={cellEditProp}
                        selectRow={selectRow}
                        striped={true}
                        hover={true}
                        condensed={true}
                        deleteRow={props.adminMode}
                        insertRow={props.adminMode}
                        search={true}
                    >

                        {columns}

                    </BootstrapTable>
                </div>
            </div>
        );
    }
}

export default LookupTable;
