import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

class UpdateModal extends React.Component {
    static propTypes = {
        updatedRows: PropTypes.object.isRequired,
    };

    isAnyRowUpdated = () => Object.keys(this.props.updatedRows).length > 0;

    render() {
        if (!this.isAnyRowUpdated()) {
            return null;
        }

        return (
            <div className="row">
                <div className="col-md-12 col-lg-12 col-sm-12">
                    <button
                        className="btn btn-small btn-primary pull-right"
                    >
                        Preview updated rows
                    </button>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    updatedRows: state.lookup.updatedRows,
});

export default connect(mapStateToProps)(UpdateModal);
