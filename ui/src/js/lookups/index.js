import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import {
    getLookup, deleteLookup, addLookup, updateLookupRow,
} from '../ducks/modules/lookup';

import SingleSelect from '../common/select/SingleSelect';
import LookupTable from './LookupTable';
import UpdateModal from './UpdateModal';

export class Lookup extends React.Component {
    static propTypes = {
        getLookup: PropTypes.func.isRequired,
        selectedLookup: PropTypes.object.isRequired,
        deleteLookup: PropTypes.func.isRequired,
    };

    static defaultProps = {
        style: null,
    };

    componentWillMount() {
        this.props.getLookup(this.props.selectedLookup);
    }

    render() {
        const props = this.props;
        const tableItems = props.data ? props.data.member : [];

        return (
            <div className="lookup" style={props.style}>

                <div className="row">
                    <div className="col-md-4 col-lg-4 col-sm-4">
                        <label>Select lookup</label>
                        <SingleSelect
                            options={props.lookups}
                            value={props.selectedLookup}
                            searchable
                            actionCallback={props.getLookup}
                        />
                    </div>
                </div>

                <LookupTable
                    data={tableItems}
                    adminMode={props.adminMode}
                    onUpdateRow={(row, name, value) =>
                        props.updateRow(row.id, name, value)}
                    onAddRow={props.addLookup}
                    onDeleteRow={(key, row) => props.deleteLookup(row[0].id)}
                    lookupValues={props.lookupValues}
                />

                <UpdateModal />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    selectedLookup: state.lookup.selectedLookup,
    lookups: state.lookup.lookups,
    data: state.lookup.data,
    adminMode: state.auth.permissions.includes('EDIT'),
    lookupValues: state.lookup.lookupValues,
});

const mapDispatchToProps = (dispatch) => ({
    getLookup: bindActionCreators(getLookup, dispatch),
    deleteLookup: bindActionCreators(deleteLookup, dispatch),
    addLookup: bindActionCreators(addLookup, dispatch),
    updateRow: bindActionCreators(updateLookupRow, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Lookup);
