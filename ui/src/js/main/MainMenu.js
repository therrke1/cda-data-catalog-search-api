import React from 'react';
import DocumentTitle from 'react-document-title';
import appInfo from '../appInfo.config.';
import Lookup from '../lookups';

const MainMenu = () => (
    <div className="container-fluid" style={{ marginTop: '1em' }}>
        <DocumentTitle title={appInfo.appTitle} />
        <Lookup />
    </div>
);

export default MainMenu;
