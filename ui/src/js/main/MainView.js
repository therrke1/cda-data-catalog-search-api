import React, { Component } from 'react';
import NxHeader from 'nxc-core/NxHeader';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Footer from '../common/Footer';
import appInfo from '../appInfo.config.';
import { getPerson } from '../ducks/modules/auth';
import Forbidden from '../common/Forbidden';

class MainView extends Component {
    static propTypes = {
        children: PropTypes.node,
    };

    static defaultProps = {
        children: undefined,
    };

    render() {
        const props = this.props;
        let children;

        if (props.fetched) {
            children = props.permissions.includes('READ')
                ? props.children
                : <Forbidden />;
        } else {
            children = null;
        }

        return (
            <div>
                <div style={{ paddingBottom: '1em' }}>
                    <NxHeader
                        appName={appInfo.appName}
                        description={appInfo.appDescription}
                        deployState={appInfo.appDeployState}
                        titleHref={'#/'}
                        onAuth={props.getPerson}
                        deployHref={''}
                        getHelpOptions={appInfo.getHelp}
                    />
                </div>
                {children}
                <Footer />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    person: state.auth.person,
    permissions: state.auth.permissions,
    fetched: state.auth.fetched,
});

const mapDispatchToProps = (dispatch) => ({
    getPerson: bindActionCreators(getPerson, dispatch),
});
export default connect(mapStateToProps, mapDispatchToProps)(MainView);
