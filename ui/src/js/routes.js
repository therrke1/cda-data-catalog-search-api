import React from 'react';
import { Route, IndexRoute } from 'react-router';

import MainView from './main/MainView';
import MainMenu from './main/MainMenu';
import NotFound from './common/NotFound';

export default (
    <Route path="/" component={MainView}>
        <IndexRoute component={MainMenu} />
        <Route path="*" component={NotFound} />
    </Route>
);
